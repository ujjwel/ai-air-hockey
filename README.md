# AI Air Hockey

## Playing air hockey using OpenAI Gym backed by neural networks

### Parts of the project:
1. gym_air_hockey : contains the driver functionality along with the neural network.
2. air_hockey : provides the OpenAI Gym environment.

### How to run:
1. Make sure all the dependencies are installed (follow readme of the above mentioned folders)
2. go to ./gym-air-hockey/gym-air-hockey/
3. Run python3 demo.py