import sys
import gym
import envs
import pylab
import pygame
import random
import numpy as np
from collections import deque
from keras.layers import Dense
from keras.optimizers import Adam
from keras.models import Sequential
from keras import backend as K
import tensorflow as tf


EPISODES = 102


# DQN agent in the cartpole example
class DQNAgent:
    def __init__(self, state_size, action_size):
        self.render = False
        self.load_model = True

        # Define size of states and actions
        self.state_size = state_size
        self.action_size = action_size

        # DQN Hyperparameters
        self.discount_factor = 0.99
        self.learning_rate = 0.001
        self.epsilon = 1.0
        self.epsilon_start, self.epsilon_end = 1.0, 0.1
        self.exploration_steps = 100000
        self.epsilon_decay_step = (self.epsilon_start-self.epsilon_end)/self.exploration_steps
        #self.epsilon_min = 0.01
        self.batch_size = 64
        self.train_start = 1000


        # Replay memory, max size 2000
        self.memory = deque(maxlen=2000)
        self.no_op_steps = 20

        # Models and Target Models
        self.model = self.build_model()
        self.target_model = self.build_model()
        self.avg_q_max = 0

        # Target Model Initialization
        self.update_target_model()

        # Tensor Board Settings
        self.sess = tf.InteractiveSession()
        K.set_session(self.sess)

        self.summary_placeholders, self.update_ops, self.summary_op = \
            self.setup_summary()
        self.summary_writer = tf.summary.FileWriter(
            'summary/airhockey_dqn', self.sess.graph)
        self.sess.run(tf.global_variables_initializer())

        if self.load_model:
            self.model.load_weights("./save_model/airhockey_dqn_trained.h5")

    # Create neural network with state as input and queue function as output
    def build_model(self):
        model = Sequential()
        model.add(Dense(24, input_dim=self.state_size, activation='relu',
                        kernel_initializer='he_uniform'))
        model.add(Dense(24, activation='relu',
                        kernel_initializer='he_uniform'))
        model.add(Dense(24, activation='relu',
                        kernel_initializer='he_uniform'))
        model.add(Dense(self.action_size, activation='linear',
                        kernel_initializer='he_uniform'))
        model.summary()
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))
        return model

    # Update the target model with the weight of the model
    def update_target_model(self):
        self.target_model.set_weights(self.model.get_weights())

    # Choosing behavior with the epsilon greed policy
    def get_action(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        else:
            q_value = self.model.predict(state)
            return np.argmax(q_value[0])

    # Store samples <s, a, r, s'> to replay memory
    def append_sample(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    # Train models with randomly extracted batches from replay memory
    def train_model(self):
        if self.epsilon > self.epsilon_end:
            self.epsilon -= self.epsilon_decay_step

        # Randomly sample the batch size in memory
        mini_batch = random.sample(self.memory, self.batch_size)

        states = np.zeros((self.batch_size, self.state_size))
        next_states = np.zeros((self.batch_size, self.state_size))
        actions, rewards, dones = [], [], []

        for i in range(self.batch_size):
            states[i] = mini_batch[i][0]
            actions.append(mini_batch[i][1])
            rewards.append(mini_batch[i][2])
            next_states[i] = mini_batch[i][3]
            dones.append(mini_batch[i][4])

        # Queue function of the model for the current state
        # Queue function of the target model for the next state
        target = self.model.predict(states)
        target_val = self.target_model.predict(next_states)

        # Update Target Using Bellman's Equation
        for i in range(self.batch_size):
            if dones[i]:
                target[i][actions[i]] = rewards[i]
            else:
                target[i][actions[i]] = rewards[i] + self.discount_factor * (
                    np.amax(target_val[i]))

        self.model.fit(states, target, batch_size=self.batch_size,
                       epochs=1, verbose=0)

# Record learning information for each episode
    def setup_summary(self):
        episode_total_reward = tf.Variable(0.)
        episode_avg_max_q = tf.Variable(0.)
        episode_duration = tf.Variable(0.)
        #episode_avg_loss = tf.Variable(0.)

        tf.summary.scalar('Total Reward/Episode', episode_total_reward)
        tf.summary.scalar('Max Q/Episode', episode_avg_max_q)
        tf.summary.scalar('step/Episode', episode_duration)
        #tf.summary.scalar('Average Loss/Episode', episode_avg_loss)

        summary_vars = [episode_total_reward, episode_avg_max_q,episode_duration] #, episode_avg_loss]
        summary_placeholders = [tf.placeholder(tf.float32) for _ in range(len(summary_vars))]
        update_ops = [summary_vars[i].assign(summary_placeholders[i]) for i in range(len(summary_vars))]
        summary_op = tf.summary.merge_all()
        return summary_placeholders, update_ops, summary_op

if __name__ == "__main__":
    # CartPole-v1 environment, maximum number of timesteps is 500
    env = gym.make('AirHockey-v0')
    # state_size = len(env.get_state())
    state = env.reset()
    state_size = 147456
    # action_size = env.action_size
    action_size = 10

    # Create DQN Agent
    agent = DQNAgent(state_size, action_size)

    scores, episodes, global_step = [], [], 0

    for e in range(EPISODES):
        print("---------------------------------------------------")
        done = False
        score, step = 0, 0

        # env initialization
        # state = env.reset()
        state = np.reshape(state, [1, -1])
        gameExit = False


        for _ in range(random.randint(1,agent.no_op_steps)):
            state, _, _, _ = env.step(8)

        state = np.reshape(state, [1, -1])

        while not done and not gameExit:
            if agent.render:
                env.render()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    gameExit = True
                    sys.exit()

            global_step += 1
            step += 1

            # Choose action to be current
            action = agent.get_action(state)
            # One time step in the environment with the selected action
            next_state, reward, done, info = env.step()
            next_state = np.reshape(next_state, [1, -1])

            # Store samples <s, a, r, s'> in replay memory
            agent.append_sample(state, action, reward, next_state, done)

            # Learn every timestep
            if len(agent.memory) >= agent.train_start:
                agent.train_model()

#            print('state = ',state)
#            print(len(state), state.shape)    
#            print('SS = ', state_size, 'AS = ', action_size)  
#            print(len(state), state.shape)
            agent.avg_q_max += np.argmax(agent.model.predict(state)[0])
            score += reward
            state = next_state

            if done:
                # Update the target model with the weight of the model for each episode
                agent.update_target_model()

                # Record learning information for each episode
                if global_step > agent.train_start:
                    stats = [score, agent.avg_q_max / float(step), step]
                             #agent.avg_loss / float(step)]
                    for i in range(len(stats)):
                        agent.sess.run(agent.update_ops[i], feed_dict={
                            agent.summary_placeholders[i]: float(stats[i])})
                    summary_str = agent.sess.run(agent.summary_op)
                    agent.summary_writer.add_summary(summary_str, e + 1)

                agent.avg_q_max = 0

                # Output training results for each episode
                scores.append(score)
                episodes.append(e)
                #pylab.plot(episodes, scores, 'b')
                #pylab.savefig("./save_graph/cartpole_dqn.png")
                print("episode:", e, "  score:", score, "  memory length:",
                      len(agent.memory), "  epsilon:", agent.epsilon)

                # Save Model Every 1000 Episodes
                if e % 10 == 0:
                    agent.model.save_weights("./save_model/airhockey_dqn_trained.h5")

