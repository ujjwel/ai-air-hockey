       гK"	  @RБw╫Abrain.Event:2рФдв      ц
╥	їoRБw╫A"Ч┼
t
dense_1_inputPlaceholder*
dtype0*)
_output_shapes
:         АА	*
shape:         АА	
m
dense_1/random_uniform/shapeConst*
valueB" @    *
dtype0*
_output_shapes
:
_
dense_1/random_uniform/minConst*
valueB
 *ь╤╗*
dtype0*
_output_shapes
: 
_
dense_1/random_uniform/maxConst*
valueB
 *ь╤;*
dtype0*
_output_shapes
: 
к
$dense_1/random_uniform/RandomUniformRandomUniformdense_1/random_uniform/shape*
T0*
dtype0* 
_output_shapes
:
АА	*
seed2їЫ╣*
seed▒ х)
z
dense_1/random_uniform/subSubdense_1/random_uniform/maxdense_1/random_uniform/min*
_output_shapes
: *
T0
О
dense_1/random_uniform/mulMul$dense_1/random_uniform/RandomUniformdense_1/random_uniform/sub* 
_output_shapes
:
АА	*
T0
А
dense_1/random_uniformAdddense_1/random_uniform/muldense_1/random_uniform/min* 
_output_shapes
:
АА	*
T0
о
dense_1/kernelVarHandleOp*
dtype0*
_output_shapes
: *
shared_namedense_1/kernel*!
_class
loc:@dense_1/kernel*
	container *
shape:
АА	
m
/dense_1/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_1/kernel*
_output_shapes
: 
Б
dense_1/kernel/AssignAssignVariableOpdense_1/kerneldense_1/random_uniform*!
_class
loc:@dense_1/kernel*
dtype0
Ц
"dense_1/kernel/Read/ReadVariableOpReadVariableOpdense_1/kernel*!
_class
loc:@dense_1/kernel*
dtype0* 
_output_shapes
:
АА	
Z
dense_1/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_1/biasVarHandleOp*
shared_namedense_1/bias*
_class
loc:@dense_1/bias*
	container *
shape:*
dtype0*
_output_shapes
: 
i
-dense_1/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_1/bias*
_output_shapes
: 
r
dense_1/bias/AssignAssignVariableOpdense_1/biasdense_1/Const*
_class
loc:@dense_1/bias*
dtype0
К
 dense_1/bias/Read/ReadVariableOpReadVariableOpdense_1/bias*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
:
n
dense_1/MatMul/ReadVariableOpReadVariableOpdense_1/kernel*
dtype0* 
_output_shapes
:
АА	
Ю
dense_1/MatMulMatMuldense_1_inputdense_1/MatMul/ReadVariableOp*'
_output_shapes
:         *
transpose_a( *
transpose_b( *
T0
g
dense_1/BiasAdd/ReadVariableOpReadVariableOpdense_1/bias*
dtype0*
_output_shapes
:
У
dense_1/BiasAddBiasAdddense_1/MatMuldense_1/BiasAdd/ReadVariableOp*
data_formatNHWC*'
_output_shapes
:         *
T0
W
dense_1/ReluReludense_1/BiasAdd*'
_output_shapes
:         *
T0
m
dense_2/random_uniform/shapeConst*
dtype0*
_output_shapes
:*
valueB"      
_
dense_2/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB
 *   ┐
_
dense_2/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_2/random_uniform/RandomUniformRandomUniformdense_2/random_uniform/shape*
T0*
dtype0*
_output_shapes

:*
seed2лВж*
seed▒ х)
z
dense_2/random_uniform/subSubdense_2/random_uniform/maxdense_2/random_uniform/min*
T0*
_output_shapes
: 
М
dense_2/random_uniform/mulMul$dense_2/random_uniform/RandomUniformdense_2/random_uniform/sub*
T0*
_output_shapes

:
~
dense_2/random_uniformAdddense_2/random_uniform/muldense_2/random_uniform/min*
T0*
_output_shapes

:
м
dense_2/kernelVarHandleOp*
shape
:*
dtype0*
_output_shapes
: *
shared_namedense_2/kernel*!
_class
loc:@dense_2/kernel*
	container 
m
/dense_2/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_2/kernel*
_output_shapes
: 
Б
dense_2/kernel/AssignAssignVariableOpdense_2/kerneldense_2/random_uniform*!
_class
loc:@dense_2/kernel*
dtype0
Ф
"dense_2/kernel/Read/ReadVariableOpReadVariableOpdense_2/kernel*!
_class
loc:@dense_2/kernel*
dtype0*
_output_shapes

:
Z
dense_2/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_2/biasVarHandleOp*
shared_namedense_2/bias*
_class
loc:@dense_2/bias*
	container *
shape:*
dtype0*
_output_shapes
: 
i
-dense_2/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_2/bias*
_output_shapes
: 
r
dense_2/bias/AssignAssignVariableOpdense_2/biasdense_2/Const*
_class
loc:@dense_2/bias*
dtype0
К
 dense_2/bias/Read/ReadVariableOpReadVariableOpdense_2/bias*
_class
loc:@dense_2/bias*
dtype0*
_output_shapes
:
l
dense_2/MatMul/ReadVariableOpReadVariableOpdense_2/kernel*
dtype0*
_output_shapes

:
Э
dense_2/MatMulMatMuldense_1/Reludense_2/MatMul/ReadVariableOp*'
_output_shapes
:         *
transpose_a( *
transpose_b( *
T0
g
dense_2/BiasAdd/ReadVariableOpReadVariableOpdense_2/bias*
dtype0*
_output_shapes
:
У
dense_2/BiasAddBiasAdddense_2/MatMuldense_2/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_2/ReluReludense_2/BiasAdd*
T0*'
_output_shapes
:         
m
dense_3/random_uniform/shapeConst*
valueB"      *
dtype0*
_output_shapes
:
_
dense_3/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_3/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB
 *   ?
и
$dense_3/random_uniform/RandomUniformRandomUniformdense_3/random_uniform/shape*
T0*
dtype0*
_output_shapes

:*
seed2яЩ╧*
seed▒ х)
z
dense_3/random_uniform/subSubdense_3/random_uniform/maxdense_3/random_uniform/min*
T0*
_output_shapes
: 
М
dense_3/random_uniform/mulMul$dense_3/random_uniform/RandomUniformdense_3/random_uniform/sub*
T0*
_output_shapes

:
~
dense_3/random_uniformAdddense_3/random_uniform/muldense_3/random_uniform/min*
T0*
_output_shapes

:
м
dense_3/kernelVarHandleOp*
	container *
shape
:*
dtype0*
_output_shapes
: *
shared_namedense_3/kernel*!
_class
loc:@dense_3/kernel
m
/dense_3/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_3/kernel*
_output_shapes
: 
Б
dense_3/kernel/AssignAssignVariableOpdense_3/kerneldense_3/random_uniform*
dtype0*!
_class
loc:@dense_3/kernel
Ф
"dense_3/kernel/Read/ReadVariableOpReadVariableOpdense_3/kernel*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes

:
Z
dense_3/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_3/biasVarHandleOp*
shared_namedense_3/bias*
_class
loc:@dense_3/bias*
	container *
shape:*
dtype0*
_output_shapes
: 
i
-dense_3/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_3/bias*
_output_shapes
: 
r
dense_3/bias/AssignAssignVariableOpdense_3/biasdense_3/Const*
_class
loc:@dense_3/bias*
dtype0
К
 dense_3/bias/Read/ReadVariableOpReadVariableOpdense_3/bias*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
:
l
dense_3/MatMul/ReadVariableOpReadVariableOpdense_3/kernel*
dtype0*
_output_shapes

:
Э
dense_3/MatMulMatMuldense_2/Reludense_3/MatMul/ReadVariableOp*
T0*'
_output_shapes
:         *
transpose_a( *
transpose_b( 
g
dense_3/BiasAdd/ReadVariableOpReadVariableOpdense_3/bias*
dtype0*
_output_shapes
:
У
dense_3/BiasAddBiasAdddense_3/MatMuldense_3/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_3/ReluReludense_3/BiasAdd*'
_output_shapes
:         *
T0
m
dense_4/random_uniform/shapeConst*
valueB"   
   *
dtype0*
_output_shapes
:
_
dense_4/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_4/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_4/random_uniform/RandomUniformRandomUniformdense_4/random_uniform/shape*
dtype0*
_output_shapes

:
*
seed2ы▓¤*
seed▒ х)*
T0
z
dense_4/random_uniform/subSubdense_4/random_uniform/maxdense_4/random_uniform/min*
_output_shapes
: *
T0
М
dense_4/random_uniform/mulMul$dense_4/random_uniform/RandomUniformdense_4/random_uniform/sub*
T0*
_output_shapes

:

~
dense_4/random_uniformAdddense_4/random_uniform/muldense_4/random_uniform/min*
_output_shapes

:
*
T0
м
dense_4/kernelVarHandleOp*!
_class
loc:@dense_4/kernel*
	container *
shape
:
*
dtype0*
_output_shapes
: *
shared_namedense_4/kernel
m
/dense_4/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_4/kernel*
_output_shapes
: 
Б
dense_4/kernel/AssignAssignVariableOpdense_4/kerneldense_4/random_uniform*!
_class
loc:@dense_4/kernel*
dtype0
Ф
"dense_4/kernel/Read/ReadVariableOpReadVariableOpdense_4/kernel*!
_class
loc:@dense_4/kernel*
dtype0*
_output_shapes

:

Z
dense_4/ConstConst*
dtype0*
_output_shapes
:
*
valueB
*    
в
dense_4/biasVarHandleOp*
_class
loc:@dense_4/bias*
	container *
shape:
*
dtype0*
_output_shapes
: *
shared_namedense_4/bias
i
-dense_4/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_4/bias*
_output_shapes
: 
r
dense_4/bias/AssignAssignVariableOpdense_4/biasdense_4/Const*
_class
loc:@dense_4/bias*
dtype0
К
 dense_4/bias/Read/ReadVariableOpReadVariableOpdense_4/bias*
_class
loc:@dense_4/bias*
dtype0*
_output_shapes
:

l
dense_4/MatMul/ReadVariableOpReadVariableOpdense_4/kernel*
dtype0*
_output_shapes

:

Э
dense_4/MatMulMatMuldense_3/Reludense_4/MatMul/ReadVariableOp*
T0*'
_output_shapes
:         
*
transpose_a( *
transpose_b( 
g
dense_4/BiasAdd/ReadVariableOpReadVariableOpdense_4/bias*
dtype0*
_output_shapes
:

У
dense_4/BiasAddBiasAdddense_4/MatMuldense_4/BiasAdd/ReadVariableOp*
data_formatNHWC*'
_output_shapes
:         
*
T0
П
)Adam/iterations/Initializer/initial_valueConst*
value	B	 R *"
_class
loc:@Adam/iterations*
dtype0	*
_output_shapes
: 
з
Adam/iterationsVarHandleOp*
dtype0	*
_output_shapes
: * 
shared_nameAdam/iterations*"
_class
loc:@Adam/iterations*
	container *
shape: 
o
0Adam/iterations/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam/iterations*
_output_shapes
: 
Ч
Adam/iterations/AssignAssignVariableOpAdam/iterations)Adam/iterations/Initializer/initial_value*"
_class
loc:@Adam/iterations*
dtype0	
П
#Adam/iterations/Read/ReadVariableOpReadVariableOpAdam/iterations*"
_class
loc:@Adam/iterations*
dtype0	*
_output_shapes
: 
Ш
,Adam/learning_rate/Initializer/initial_valueConst*
dtype0*
_output_shapes
: *
valueB
 *oГ:*%
_class
loc:@Adam/learning_rate
░
Adam/learning_rateVarHandleOp*
	container *
shape: *
dtype0*
_output_shapes
: *#
shared_nameAdam/learning_rate*%
_class
loc:@Adam/learning_rate
u
3Adam/learning_rate/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam/learning_rate*
_output_shapes
: 
г
Adam/learning_rate/AssignAssignVariableOpAdam/learning_rate,Adam/learning_rate/Initializer/initial_value*%
_class
loc:@Adam/learning_rate*
dtype0
Ш
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*%
_class
loc:@Adam/learning_rate*
dtype0*
_output_shapes
: 
К
%Adam/beta_1/Initializer/initial_valueConst*
valueB
 *fff?*
_class
loc:@Adam/beta_1*
dtype0*
_output_shapes
: 
Ы
Adam/beta_1VarHandleOp*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_nameAdam/beta_1*
_class
loc:@Adam/beta_1
g
,Adam/beta_1/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam/beta_1*
_output_shapes
: 
З
Adam/beta_1/AssignAssignVariableOpAdam/beta_1%Adam/beta_1/Initializer/initial_value*
_class
loc:@Adam/beta_1*
dtype0
Г
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_class
loc:@Adam/beta_1*
dtype0*
_output_shapes
: 
К
%Adam/beta_2/Initializer/initial_valueConst*
valueB
 *w╛?*
_class
loc:@Adam/beta_2*
dtype0*
_output_shapes
: 
Ы
Adam/beta_2VarHandleOp*
dtype0*
_output_shapes
: *
shared_nameAdam/beta_2*
_class
loc:@Adam/beta_2*
	container *
shape: 
g
,Adam/beta_2/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam/beta_2*
_output_shapes
: 
З
Adam/beta_2/AssignAssignVariableOpAdam/beta_2%Adam/beta_2/Initializer/initial_value*
_class
loc:@Adam/beta_2*
dtype0
Г
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_class
loc:@Adam/beta_2*
dtype0*
_output_shapes
: 
И
$Adam/decay/Initializer/initial_valueConst*
dtype0*
_output_shapes
: *
valueB
 *    *
_class
loc:@Adam/decay
Ш

Adam/decayVarHandleOp*
shared_name
Adam/decay*
_class
loc:@Adam/decay*
	container *
shape: *
dtype0*
_output_shapes
: 
e
+Adam/decay/IsInitialized/VarIsInitializedOpVarIsInitializedOp
Adam/decay*
_output_shapes
: 
Г
Adam/decay/AssignAssignVariableOp
Adam/decay$Adam/decay/Initializer/initial_value*
_class
loc:@Adam/decay*
dtype0
А
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_class
loc:@Adam/decay*
dtype0*
_output_shapes
: 
Г
dense_4_targetPlaceholder*
dtype0*0
_output_shapes
:                  *%
shape:                  
q
dense_4_sample_weightsPlaceholder*
dtype0*#
_output_shapes
:         *
shape:         
В
(loss/dense_4_loss/mean_squared_error/subSubdense_4/BiasAdddense_4_target*
T0*'
_output_shapes
:         

С
+loss/dense_4_loss/mean_squared_error/SquareSquare(loss/dense_4_loss/mean_squared_error/sub*
T0*'
_output_shapes
:         

Ж
;loss/dense_4_loss/mean_squared_error/Mean/reduction_indicesConst*
valueB :
         *
dtype0*
_output_shapes
: 
ц
)loss/dense_4_loss/mean_squared_error/MeanMean+loss/dense_4_loss/mean_squared_error/Square;loss/dense_4_loss/mean_squared_error/Mean/reduction_indices*#
_output_shapes
:         *
	keep_dims( *

Tidx0*
T0
о
6loss/dense_4_loss/mean_squared_error/weighted_loss/mulMuldense_4_sample_weights)loss/dense_4_loss/mean_squared_error/Mean*
T0*#
_output_shapes
:         
В
8loss/dense_4_loss/mean_squared_error/weighted_loss/ConstConst*
valueB: *
dtype0*
_output_shapes
:
э
6loss/dense_4_loss/mean_squared_error/weighted_loss/SumSum6loss/dense_4_loss/mean_squared_error/weighted_loss/mul8loss/dense_4_loss/mean_squared_error/weighted_loss/Const*
_output_shapes
: *
	keep_dims( *

Tidx0*
T0
╡
Dloss/dense_4_loss/mean_squared_error/weighted_loss/num_elements/SizeSize6loss/dense_4_loss/mean_squared_error/weighted_loss/mul*
_output_shapes
: *
T0*
out_type0
╥
Dloss/dense_4_loss/mean_squared_error/weighted_loss/num_elements/CastCastDloss/dense_4_loss/mean_squared_error/weighted_loss/num_elements/Size*

SrcT0*
Truncate( *
_output_shapes
: *

DstT0
ф
:loss/dense_4_loss/mean_squared_error/weighted_loss/truedivRealDiv6loss/dense_4_loss/mean_squared_error/weighted_loss/SumDloss/dense_4_loss/mean_squared_error/weighted_loss/num_elements/Cast*
T0*
_output_shapes
: 
O

loss/mul/xConst*
dtype0*
_output_shapes
: *
valueB
 *  А?
x
loss/mulMul
loss/mul/x:loss/dense_4_loss/mean_squared_error/weighted_loss/truediv*
T0*
_output_shapes
: 
H
ConstConst*
valueB *
dtype0*
_output_shapes
: 
[
MeanMeanloss/mulConst*
T0*
_output_shapes
: *
	keep_dims( *

Tidx0
t
dense_5_inputPlaceholder*
dtype0*)
_output_shapes
:         АА	*
shape:         АА	
m
dense_5/random_uniform/shapeConst*
valueB" @    *
dtype0*
_output_shapes
:
_
dense_5/random_uniform/minConst*
valueB
 *ь╤╗*
dtype0*
_output_shapes
: 
_
dense_5/random_uniform/maxConst*
valueB
 *ь╤;*
dtype0*
_output_shapes
: 
к
$dense_5/random_uniform/RandomUniformRandomUniformdense_5/random_uniform/shape*
T0*
dtype0* 
_output_shapes
:
АА	*
seed2│ъ▀*
seed▒ х)
z
dense_5/random_uniform/subSubdense_5/random_uniform/maxdense_5/random_uniform/min*
T0*
_output_shapes
: 
О
dense_5/random_uniform/mulMul$dense_5/random_uniform/RandomUniformdense_5/random_uniform/sub*
T0* 
_output_shapes
:
АА	
А
dense_5/random_uniformAdddense_5/random_uniform/muldense_5/random_uniform/min*
T0* 
_output_shapes
:
АА	
о
dense_5/kernelVarHandleOp*
dtype0*
_output_shapes
: *
shared_namedense_5/kernel*!
_class
loc:@dense_5/kernel*
	container *
shape:
АА	
m
/dense_5/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_5/kernel*
_output_shapes
: 
Б
dense_5/kernel/AssignAssignVariableOpdense_5/kerneldense_5/random_uniform*
dtype0*!
_class
loc:@dense_5/kernel
Ц
"dense_5/kernel/Read/ReadVariableOpReadVariableOpdense_5/kernel*!
_class
loc:@dense_5/kernel*
dtype0* 
_output_shapes
:
АА	
Z
dense_5/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_5/biasVarHandleOp*
shared_namedense_5/bias*
_class
loc:@dense_5/bias*
	container *
shape:*
dtype0*
_output_shapes
: 
i
-dense_5/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_5/bias*
_output_shapes
: 
r
dense_5/bias/AssignAssignVariableOpdense_5/biasdense_5/Const*
_class
loc:@dense_5/bias*
dtype0
К
 dense_5/bias/Read/ReadVariableOpReadVariableOpdense_5/bias*
dtype0*
_output_shapes
:*
_class
loc:@dense_5/bias
n
dense_5/MatMul/ReadVariableOpReadVariableOpdense_5/kernel*
dtype0* 
_output_shapes
:
АА	
Ю
dense_5/MatMulMatMuldense_5_inputdense_5/MatMul/ReadVariableOp*'
_output_shapes
:         *
transpose_a( *
transpose_b( *
T0
g
dense_5/BiasAdd/ReadVariableOpReadVariableOpdense_5/bias*
dtype0*
_output_shapes
:
У
dense_5/BiasAddBiasAdddense_5/MatMuldense_5/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_5/ReluReludense_5/BiasAdd*
T0*'
_output_shapes
:         
m
dense_6/random_uniform/shapeConst*
dtype0*
_output_shapes
:*
valueB"      
_
dense_6/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_6/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_6/random_uniform/RandomUniformRandomUniformdense_6/random_uniform/shape*
T0*
dtype0*
_output_shapes

:*
seed2┌╓ы*
seed▒ х)
z
dense_6/random_uniform/subSubdense_6/random_uniform/maxdense_6/random_uniform/min*
T0*
_output_shapes
: 
М
dense_6/random_uniform/mulMul$dense_6/random_uniform/RandomUniformdense_6/random_uniform/sub*
T0*
_output_shapes

:
~
dense_6/random_uniformAdddense_6/random_uniform/muldense_6/random_uniform/min*
T0*
_output_shapes

:
м
dense_6/kernelVarHandleOp*
shared_namedense_6/kernel*!
_class
loc:@dense_6/kernel*
	container *
shape
:*
dtype0*
_output_shapes
: 
m
/dense_6/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_6/kernel*
_output_shapes
: 
Б
dense_6/kernel/AssignAssignVariableOpdense_6/kerneldense_6/random_uniform*!
_class
loc:@dense_6/kernel*
dtype0
Ф
"dense_6/kernel/Read/ReadVariableOpReadVariableOpdense_6/kernel*
dtype0*
_output_shapes

:*!
_class
loc:@dense_6/kernel
Z
dense_6/ConstConst*
dtype0*
_output_shapes
:*
valueB*    
в
dense_6/biasVarHandleOp*
dtype0*
_output_shapes
: *
shared_namedense_6/bias*
_class
loc:@dense_6/bias*
	container *
shape:
i
-dense_6/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_6/bias*
_output_shapes
: 
r
dense_6/bias/AssignAssignVariableOpdense_6/biasdense_6/Const*
_class
loc:@dense_6/bias*
dtype0
К
 dense_6/bias/Read/ReadVariableOpReadVariableOpdense_6/bias*
dtype0*
_output_shapes
:*
_class
loc:@dense_6/bias
l
dense_6/MatMul/ReadVariableOpReadVariableOpdense_6/kernel*
dtype0*
_output_shapes

:
Э
dense_6/MatMulMatMuldense_5/Reludense_6/MatMul/ReadVariableOp*
T0*'
_output_shapes
:         *
transpose_a( *
transpose_b( 
g
dense_6/BiasAdd/ReadVariableOpReadVariableOpdense_6/bias*
dtype0*
_output_shapes
:
У
dense_6/BiasAddBiasAdddense_6/MatMuldense_6/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_6/ReluReludense_6/BiasAdd*
T0*'
_output_shapes
:         
m
dense_7/random_uniform/shapeConst*
valueB"      *
dtype0*
_output_shapes
:
_
dense_7/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_7/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_7/random_uniform/RandomUniformRandomUniformdense_7/random_uniform/shape*
T0*
dtype0*
_output_shapes

:*
seed2ЯВ■*
seed▒ х)
z
dense_7/random_uniform/subSubdense_7/random_uniform/maxdense_7/random_uniform/min*
T0*
_output_shapes
: 
М
dense_7/random_uniform/mulMul$dense_7/random_uniform/RandomUniformdense_7/random_uniform/sub*
_output_shapes

:*
T0
~
dense_7/random_uniformAdddense_7/random_uniform/muldense_7/random_uniform/min*
T0*
_output_shapes

:
м
dense_7/kernelVarHandleOp*
	container *
shape
:*
dtype0*
_output_shapes
: *
shared_namedense_7/kernel*!
_class
loc:@dense_7/kernel
m
/dense_7/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_7/kernel*
_output_shapes
: 
Б
dense_7/kernel/AssignAssignVariableOpdense_7/kerneldense_7/random_uniform*!
_class
loc:@dense_7/kernel*
dtype0
Ф
"dense_7/kernel/Read/ReadVariableOpReadVariableOpdense_7/kernel*!
_class
loc:@dense_7/kernel*
dtype0*
_output_shapes

:
Z
dense_7/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_7/biasVarHandleOp*
	container *
shape:*
dtype0*
_output_shapes
: *
shared_namedense_7/bias*
_class
loc:@dense_7/bias
i
-dense_7/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_7/bias*
_output_shapes
: 
r
dense_7/bias/AssignAssignVariableOpdense_7/biasdense_7/Const*
_class
loc:@dense_7/bias*
dtype0
К
 dense_7/bias/Read/ReadVariableOpReadVariableOpdense_7/bias*
_class
loc:@dense_7/bias*
dtype0*
_output_shapes
:
l
dense_7/MatMul/ReadVariableOpReadVariableOpdense_7/kernel*
dtype0*
_output_shapes

:
Э
dense_7/MatMulMatMuldense_6/Reludense_7/MatMul/ReadVariableOp*
T0*'
_output_shapes
:         *
transpose_a( *
transpose_b( 
g
dense_7/BiasAdd/ReadVariableOpReadVariableOpdense_7/bias*
dtype0*
_output_shapes
:
У
dense_7/BiasAddBiasAdddense_7/MatMuldense_7/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_7/ReluReludense_7/BiasAdd*
T0*'
_output_shapes
:         
m
dense_8/random_uniform/shapeConst*
valueB"   
   *
dtype0*
_output_shapes
:
_
dense_8/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_8/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_8/random_uniform/RandomUniformRandomUniformdense_8/random_uniform/shape*
seed▒ х)*
T0*
dtype0*
_output_shapes

:
*
seed2▓п╥
z
dense_8/random_uniform/subSubdense_8/random_uniform/maxdense_8/random_uniform/min*
_output_shapes
: *
T0
М
dense_8/random_uniform/mulMul$dense_8/random_uniform/RandomUniformdense_8/random_uniform/sub*
T0*
_output_shapes

:

~
dense_8/random_uniformAdddense_8/random_uniform/muldense_8/random_uniform/min*
T0*
_output_shapes

:

м
dense_8/kernelVarHandleOp*
shared_namedense_8/kernel*!
_class
loc:@dense_8/kernel*
	container *
shape
:
*
dtype0*
_output_shapes
: 
m
/dense_8/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_8/kernel*
_output_shapes
: 
Б
dense_8/kernel/AssignAssignVariableOpdense_8/kerneldense_8/random_uniform*!
_class
loc:@dense_8/kernel*
dtype0
Ф
"dense_8/kernel/Read/ReadVariableOpReadVariableOpdense_8/kernel*!
_class
loc:@dense_8/kernel*
dtype0*
_output_shapes

:

Z
dense_8/ConstConst*
dtype0*
_output_shapes
:
*
valueB
*    
в
dense_8/biasVarHandleOp*
shared_namedense_8/bias*
_class
loc:@dense_8/bias*
	container *
shape:
*
dtype0*
_output_shapes
: 
i
-dense_8/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_8/bias*
_output_shapes
: 
r
dense_8/bias/AssignAssignVariableOpdense_8/biasdense_8/Const*
_class
loc:@dense_8/bias*
dtype0
К
 dense_8/bias/Read/ReadVariableOpReadVariableOpdense_8/bias*
_class
loc:@dense_8/bias*
dtype0*
_output_shapes
:

l
dense_8/MatMul/ReadVariableOpReadVariableOpdense_8/kernel*
dtype0*
_output_shapes

:

Э
dense_8/MatMulMatMuldense_7/Reludense_8/MatMul/ReadVariableOp*
transpose_b( *
T0*'
_output_shapes
:         
*
transpose_a( 
g
dense_8/BiasAdd/ReadVariableOpReadVariableOpdense_8/bias*
dtype0*
_output_shapes
:

У
dense_8/BiasAddBiasAdddense_8/MatMuldense_8/BiasAdd/ReadVariableOp*
data_formatNHWC*'
_output_shapes
:         
*
T0
У
+Adam_1/iterations/Initializer/initial_valueConst*
value	B	 R *$
_class
loc:@Adam_1/iterations*
dtype0	*
_output_shapes
: 
н
Adam_1/iterationsVarHandleOp*
shape: *
dtype0	*
_output_shapes
: *"
shared_nameAdam_1/iterations*$
_class
loc:@Adam_1/iterations*
	container 
s
2Adam_1/iterations/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/iterations*
_output_shapes
: 
Я
Adam_1/iterations/AssignAssignVariableOpAdam_1/iterations+Adam_1/iterations/Initializer/initial_value*$
_class
loc:@Adam_1/iterations*
dtype0	
Х
%Adam_1/iterations/Read/ReadVariableOpReadVariableOpAdam_1/iterations*
dtype0	*
_output_shapes
: *$
_class
loc:@Adam_1/iterations
Ь
.Adam_1/learning_rate/Initializer/initial_valueConst*
valueB
 *oГ:*'
_class
loc:@Adam_1/learning_rate*
dtype0*
_output_shapes
: 
╢
Adam_1/learning_rateVarHandleOp*
shape: *
dtype0*
_output_shapes
: *%
shared_nameAdam_1/learning_rate*'
_class
loc:@Adam_1/learning_rate*
	container 
y
5Adam_1/learning_rate/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/learning_rate*
_output_shapes
: 
л
Adam_1/learning_rate/AssignAssignVariableOpAdam_1/learning_rate.Adam_1/learning_rate/Initializer/initial_value*'
_class
loc:@Adam_1/learning_rate*
dtype0
Ю
(Adam_1/learning_rate/Read/ReadVariableOpReadVariableOpAdam_1/learning_rate*'
_class
loc:@Adam_1/learning_rate*
dtype0*
_output_shapes
: 
О
'Adam_1/beta_1/Initializer/initial_valueConst*
valueB
 *fff?* 
_class
loc:@Adam_1/beta_1*
dtype0*
_output_shapes
: 
б
Adam_1/beta_1VarHandleOp*
shared_nameAdam_1/beta_1* 
_class
loc:@Adam_1/beta_1*
	container *
shape: *
dtype0*
_output_shapes
: 
k
.Adam_1/beta_1/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/beta_1*
_output_shapes
: 
П
Adam_1/beta_1/AssignAssignVariableOpAdam_1/beta_1'Adam_1/beta_1/Initializer/initial_value* 
_class
loc:@Adam_1/beta_1*
dtype0
Й
!Adam_1/beta_1/Read/ReadVariableOpReadVariableOpAdam_1/beta_1* 
_class
loc:@Adam_1/beta_1*
dtype0*
_output_shapes
: 
О
'Adam_1/beta_2/Initializer/initial_valueConst*
valueB
 *w╛?* 
_class
loc:@Adam_1/beta_2*
dtype0*
_output_shapes
: 
б
Adam_1/beta_2VarHandleOp*
shape: *
dtype0*
_output_shapes
: *
shared_nameAdam_1/beta_2* 
_class
loc:@Adam_1/beta_2*
	container 
k
.Adam_1/beta_2/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/beta_2*
_output_shapes
: 
П
Adam_1/beta_2/AssignAssignVariableOpAdam_1/beta_2'Adam_1/beta_2/Initializer/initial_value*
dtype0* 
_class
loc:@Adam_1/beta_2
Й
!Adam_1/beta_2/Read/ReadVariableOpReadVariableOpAdam_1/beta_2* 
_class
loc:@Adam_1/beta_2*
dtype0*
_output_shapes
: 
М
&Adam_1/decay/Initializer/initial_valueConst*
dtype0*
_output_shapes
: *
valueB
 *    *
_class
loc:@Adam_1/decay
Ю
Adam_1/decayVarHandleOp*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_nameAdam_1/decay*
_class
loc:@Adam_1/decay
i
-Adam_1/decay/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/decay*
_output_shapes
: 
Л
Adam_1/decay/AssignAssignVariableOpAdam_1/decay&Adam_1/decay/Initializer/initial_value*
_class
loc:@Adam_1/decay*
dtype0
Ж
 Adam_1/decay/Read/ReadVariableOpReadVariableOpAdam_1/decay*
_class
loc:@Adam_1/decay*
dtype0*
_output_shapes
: 
Г
dense_8_targetPlaceholder*
dtype0*0
_output_shapes
:                  *%
shape:                  
q
dense_8_sample_weightsPlaceholder*
dtype0*#
_output_shapes
:         *
shape:         
Д
*loss_1/dense_8_loss/mean_squared_error/subSubdense_8/BiasAdddense_8_target*
T0*'
_output_shapes
:         

Х
-loss_1/dense_8_loss/mean_squared_error/SquareSquare*loss_1/dense_8_loss/mean_squared_error/sub*'
_output_shapes
:         
*
T0
И
=loss_1/dense_8_loss/mean_squared_error/Mean/reduction_indicesConst*
valueB :
         *
dtype0*
_output_shapes
: 
ь
+loss_1/dense_8_loss/mean_squared_error/MeanMean-loss_1/dense_8_loss/mean_squared_error/Square=loss_1/dense_8_loss/mean_squared_error/Mean/reduction_indices*
T0*#
_output_shapes
:         *
	keep_dims( *

Tidx0
▓
8loss_1/dense_8_loss/mean_squared_error/weighted_loss/mulMuldense_8_sample_weights+loss_1/dense_8_loss/mean_squared_error/Mean*
T0*#
_output_shapes
:         
Д
:loss_1/dense_8_loss/mean_squared_error/weighted_loss/ConstConst*
dtype0*
_output_shapes
:*
valueB: 
є
8loss_1/dense_8_loss/mean_squared_error/weighted_loss/SumSum8loss_1/dense_8_loss/mean_squared_error/weighted_loss/mul:loss_1/dense_8_loss/mean_squared_error/weighted_loss/Const*
T0*
_output_shapes
: *
	keep_dims( *

Tidx0
╣
Floss_1/dense_8_loss/mean_squared_error/weighted_loss/num_elements/SizeSize8loss_1/dense_8_loss/mean_squared_error/weighted_loss/mul*
T0*
out_type0*
_output_shapes
: 
╓
Floss_1/dense_8_loss/mean_squared_error/weighted_loss/num_elements/CastCastFloss_1/dense_8_loss/mean_squared_error/weighted_loss/num_elements/Size*

SrcT0*
Truncate( *
_output_shapes
: *

DstT0
ъ
<loss_1/dense_8_loss/mean_squared_error/weighted_loss/truedivRealDiv8loss_1/dense_8_loss/mean_squared_error/weighted_loss/SumFloss_1/dense_8_loss/mean_squared_error/weighted_loss/num_elements/Cast*
T0*
_output_shapes
: 
Q
loss_1/mul/xConst*
valueB
 *  А?*
dtype0*
_output_shapes
: 
~

loss_1/mulMulloss_1/mul/x<loss_1/dense_8_loss/mean_squared_error/weighted_loss/truediv*
_output_shapes
: *
T0
J
Const_1Const*
valueB *
dtype0*
_output_shapes
: 
a
Mean_1Mean
loss_1/mulConst_1*
_output_shapes
: *
	keep_dims( *

Tidx0*
T0
L
VarIsInitializedOpVarIsInitializedOp
Adam/decay*
_output_shapes
: 
Q
VarIsInitializedOp_1VarIsInitializedOpAdam_1/beta_2*
_output_shapes
: 
P
VarIsInitializedOp_2VarIsInitializedOpdense_5/bias*
_output_shapes
: 
P
VarIsInitializedOp_3VarIsInitializedOpdense_8/bias*
_output_shapes
: 
R
VarIsInitializedOp_4VarIsInitializedOpdense_6/kernel*
_output_shapes
: 
P
VarIsInitializedOp_5VarIsInitializedOpdense_6/bias*
_output_shapes
: 
O
VarIsInitializedOp_6VarIsInitializedOpAdam/beta_2*
_output_shapes
: 
R
VarIsInitializedOp_7VarIsInitializedOpdense_2/kernel*
_output_shapes
: 
P
VarIsInitializedOp_8VarIsInitializedOpdense_3/bias*
_output_shapes
: 
V
VarIsInitializedOp_9VarIsInitializedOpAdam/learning_rate*
_output_shapes
: 
S
VarIsInitializedOp_10VarIsInitializedOpdense_7/kernel*
_output_shapes
: 
S
VarIsInitializedOp_11VarIsInitializedOpdense_4/kernel*
_output_shapes
: 
Q
VarIsInitializedOp_12VarIsInitializedOpdense_1/bias*
_output_shapes
: 
S
VarIsInitializedOp_13VarIsInitializedOpdense_1/kernel*
_output_shapes
: 
Q
VarIsInitializedOp_14VarIsInitializedOpdense_4/bias*
_output_shapes
: 
S
VarIsInitializedOp_15VarIsInitializedOpdense_8/kernel*
_output_shapes
: 
Q
VarIsInitializedOp_16VarIsInitializedOpAdam_1/decay*
_output_shapes
: 
Q
VarIsInitializedOp_17VarIsInitializedOpdense_7/bias*
_output_shapes
: 
V
VarIsInitializedOp_18VarIsInitializedOpAdam_1/iterations*
_output_shapes
: 
Y
VarIsInitializedOp_19VarIsInitializedOpAdam_1/learning_rate*
_output_shapes
: 
Q
VarIsInitializedOp_20VarIsInitializedOpdense_2/bias*
_output_shapes
: 
P
VarIsInitializedOp_21VarIsInitializedOpAdam/beta_1*
_output_shapes
: 
S
VarIsInitializedOp_22VarIsInitializedOpdense_3/kernel*
_output_shapes
: 
T
VarIsInitializedOp_23VarIsInitializedOpAdam/iterations*
_output_shapes
: 
S
VarIsInitializedOp_24VarIsInitializedOpdense_5/kernel*
_output_shapes
: 
R
VarIsInitializedOp_25VarIsInitializedOpAdam_1/beta_1*
_output_shapes
: 
ь
initNoOp^Adam/beta_1/Assign^Adam/beta_2/Assign^Adam/decay/Assign^Adam/iterations/Assign^Adam/learning_rate/Assign^Adam_1/beta_1/Assign^Adam_1/beta_2/Assign^Adam_1/decay/Assign^Adam_1/iterations/Assign^Adam_1/learning_rate/Assign^dense_1/bias/Assign^dense_1/kernel/Assign^dense_2/bias/Assign^dense_2/kernel/Assign^dense_3/bias/Assign^dense_3/kernel/Assign^dense_4/bias/Assign^dense_4/kernel/Assign^dense_5/bias/Assign^dense_5/kernel/Assign^dense_6/bias/Assign^dense_6/kernel/Assign^dense_7/bias/Assign^dense_7/kernel/Assign^dense_8/bias/Assign^dense_8/kernel/Assign
`
PlaceholderPlaceholder*
dtype0* 
_output_shapes
:
АА	*
shape:
АА	
N
AssignVariableOpAssignVariableOpdense_5/kernelPlaceholder*
dtype0
r
ReadVariableOpReadVariableOpdense_5/kernel^AssignVariableOp*
dtype0* 
_output_shapes
:
АА	
V
Placeholder_1Placeholder*
shape:*
dtype0*
_output_shapes
:
P
AssignVariableOp_1AssignVariableOpdense_5/biasPlaceholder_1*
dtype0
n
ReadVariableOp_1ReadVariableOpdense_5/bias^AssignVariableOp_1*
dtype0*
_output_shapes
:
^
Placeholder_2Placeholder*
dtype0*
_output_shapes

:*
shape
:
R
AssignVariableOp_2AssignVariableOpdense_6/kernelPlaceholder_2*
dtype0
t
ReadVariableOp_2ReadVariableOpdense_6/kernel^AssignVariableOp_2*
dtype0*
_output_shapes

:
V
Placeholder_3Placeholder*
dtype0*
_output_shapes
:*
shape:
P
AssignVariableOp_3AssignVariableOpdense_6/biasPlaceholder_3*
dtype0
n
ReadVariableOp_3ReadVariableOpdense_6/bias^AssignVariableOp_3*
dtype0*
_output_shapes
:
^
Placeholder_4Placeholder*
shape
:*
dtype0*
_output_shapes

:
R
AssignVariableOp_4AssignVariableOpdense_7/kernelPlaceholder_4*
dtype0
t
ReadVariableOp_4ReadVariableOpdense_7/kernel^AssignVariableOp_4*
dtype0*
_output_shapes

:
V
Placeholder_5Placeholder*
shape:*
dtype0*
_output_shapes
:
P
AssignVariableOp_5AssignVariableOpdense_7/biasPlaceholder_5*
dtype0
n
ReadVariableOp_5ReadVariableOpdense_7/bias^AssignVariableOp_5*
dtype0*
_output_shapes
:
^
Placeholder_6Placeholder*
dtype0*
_output_shapes

:
*
shape
:

R
AssignVariableOp_6AssignVariableOpdense_8/kernelPlaceholder_6*
dtype0
t
ReadVariableOp_6ReadVariableOpdense_8/kernel^AssignVariableOp_6*
dtype0*
_output_shapes

:

V
Placeholder_7Placeholder*
shape:
*
dtype0*
_output_shapes
:

P
AssignVariableOp_7AssignVariableOpdense_8/biasPlaceholder_7*
dtype0
n
ReadVariableOp_7ReadVariableOpdense_8/bias^AssignVariableOp_7*
dtype0*
_output_shapes
:

[
Variable/initial_valueConst*
valueB
 *    *
dtype0*
_output_shapes
: 
l
Variable
VariableV2*
dtype0*
_output_shapes
: *
	container *
shape: *
shared_name 
в
Variable/AssignAssignVariableVariable/initial_value*
use_locking(*
T0*
_class
loc:@Variable*
validate_shape(*
_output_shapes
: 
a
Variable/readIdentityVariable*
T0*
_class
loc:@Variable*
_output_shapes
: 
]
Variable_1/initial_valueConst*
valueB
 *    *
dtype0*
_output_shapes
: 
n

Variable_1
VariableV2*
dtype0*
_output_shapes
: *
	container *
shape: *
shared_name 
к
Variable_1/AssignAssign
Variable_1Variable_1/initial_value*
use_locking(*
T0*
_class
loc:@Variable_1*
validate_shape(*
_output_shapes
: 
g
Variable_1/readIdentity
Variable_1*
T0*
_class
loc:@Variable_1*
_output_shapes
: 
]
Variable_2/initial_valueConst*
valueB
 *    *
dtype0*
_output_shapes
: 
n

Variable_2
VariableV2*
dtype0*
_output_shapes
: *
	container *
shape: *
shared_name 
к
Variable_2/AssignAssign
Variable_2Variable_2/initial_value*
use_locking(*
T0*
_class
loc:@Variable_2*
validate_shape(*
_output_shapes
: 
g
Variable_2/readIdentity
Variable_2*
T0*
_class
loc:@Variable_2*
_output_shapes
: 
n
Total_Reward/Episode/tagsConst*%
valueB BTotal_Reward/Episode*
dtype0*
_output_shapes
: 
p
Total_Reward/EpisodeScalarSummaryTotal_Reward/Episode/tagsVariable/read*
T0*
_output_shapes
: 
`
Max_Q/Episode/tagsConst*
valueB BMax_Q/Episode*
dtype0*
_output_shapes
: 
d
Max_Q/EpisodeScalarSummaryMax_Q/Episode/tagsVariable_1/read*
T0*
_output_shapes
: 
^
step/Episode/tagsConst*
valueB Bstep/Episode*
dtype0*
_output_shapes
: 
b
step/EpisodeScalarSummarystep/Episode/tagsVariable_2/read*
_output_shapes
: *
T0
R
Placeholder_8Placeholder*
dtype0*
_output_shapes
:*
shape:
R
Placeholder_9Placeholder*
dtype0*
_output_shapes
:*
shape:
S
Placeholder_10Placeholder*
dtype0*
_output_shapes
:*
shape:
Р
AssignAssignVariablePlaceholder_8*
validate_shape(*
_output_shapes
: *
use_locking( *
T0*
_class
loc:@Variable
Ц
Assign_1Assign
Variable_1Placeholder_9*
use_locking( *
T0*
_class
loc:@Variable_1*
validate_shape(*
_output_shapes
: 
Ч
Assign_2Assign
Variable_2Placeholder_10*
T0*
_class
loc:@Variable_2*
validate_shape(*
_output_shapes
: *
use_locking( 
v
Merge/MergeSummaryMergeSummaryTotal_Reward/EpisodeMax_Q/Episodestep/Episode*
N*
_output_shapes
: "&пS└L─      Fй$		N╟pRБw╫AJ┐И
▒К
:
Add
x"T
y"T
z"T"
Ttype:
2	
x
Assign
ref"TА

value"T

output_ref"TА"	
Ttype"
validate_shapebool("
use_lockingbool(Ш
B
AssignVariableOp
resource
value"dtype"
dtypetypeИ
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
Н
Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
8
MergeSummary
inputs*N
summary"
Nint(0
=
Mul
x"T
y"T
z"T"
Ttype:
2	Р

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
~
RandomUniform

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	И
@
ReadVariableOp
resource
value"dtype"
dtypetypeИ
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
P
ScalarSummary
tags
values"T
summary"
Ttype:
2	
O
Size

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
1
Square
x"T
y"T"
Ttype:

2	
:
Sub
x"T
y"T
z"T"
Ttype:
2	
М
Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshapeИ
9
VarIsInitializedOp
resource
is_initialized
И
s

VariableV2
ref"dtypeА"
shapeshape"
dtypetype"
	containerstring "
shared_namestring И*1.14.02v1.14.0-rc1-22-gaf24dc91b5Ч┼
t
dense_1_inputPlaceholder*
dtype0*)
_output_shapes
:         АА	*
shape:         АА	
m
dense_1/random_uniform/shapeConst*
valueB" @    *
dtype0*
_output_shapes
:
_
dense_1/random_uniform/minConst*
valueB
 *ь╤╗*
dtype0*
_output_shapes
: 
_
dense_1/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB
 *ь╤;
к
$dense_1/random_uniform/RandomUniformRandomUniformdense_1/random_uniform/shape*
T0*
dtype0*
seed2їЫ╣* 
_output_shapes
:
АА	*
seed▒ х)
z
dense_1/random_uniform/subSubdense_1/random_uniform/maxdense_1/random_uniform/min*
T0*
_output_shapes
: 
О
dense_1/random_uniform/mulMul$dense_1/random_uniform/RandomUniformdense_1/random_uniform/sub*
T0* 
_output_shapes
:
АА	
А
dense_1/random_uniformAdddense_1/random_uniform/muldense_1/random_uniform/min*
T0* 
_output_shapes
:
АА	
о
dense_1/kernelVarHandleOp*
	container *
shape:
АА	*
dtype0*
_output_shapes
: *
shared_namedense_1/kernel*!
_class
loc:@dense_1/kernel
m
/dense_1/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_1/kernel*
_output_shapes
: 
Б
dense_1/kernel/AssignAssignVariableOpdense_1/kerneldense_1/random_uniform*!
_class
loc:@dense_1/kernel*
dtype0
Ц
"dense_1/kernel/Read/ReadVariableOpReadVariableOpdense_1/kernel*!
_class
loc:@dense_1/kernel*
dtype0* 
_output_shapes
:
АА	
Z
dense_1/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_1/biasVarHandleOp*
shape:*
dtype0*
_output_shapes
: *
shared_namedense_1/bias*
_class
loc:@dense_1/bias*
	container 
i
-dense_1/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_1/bias*
_output_shapes
: 
r
dense_1/bias/AssignAssignVariableOpdense_1/biasdense_1/Const*
_class
loc:@dense_1/bias*
dtype0
К
 dense_1/bias/Read/ReadVariableOpReadVariableOpdense_1/bias*
_class
loc:@dense_1/bias*
dtype0*
_output_shapes
:
n
dense_1/MatMul/ReadVariableOpReadVariableOpdense_1/kernel*
dtype0* 
_output_shapes
:
АА	
Ю
dense_1/MatMulMatMuldense_1_inputdense_1/MatMul/ReadVariableOp*
transpose_a( *'
_output_shapes
:         *
transpose_b( *
T0
g
dense_1/BiasAdd/ReadVariableOpReadVariableOpdense_1/bias*
dtype0*
_output_shapes
:
У
dense_1/BiasAddBiasAdddense_1/MatMuldense_1/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_1/ReluReludense_1/BiasAdd*
T0*'
_output_shapes
:         
m
dense_2/random_uniform/shapeConst*
valueB"      *
dtype0*
_output_shapes
:
_
dense_2/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_2/random_uniform/maxConst*
dtype0*
_output_shapes
: *
valueB
 *   ?
и
$dense_2/random_uniform/RandomUniformRandomUniformdense_2/random_uniform/shape*
T0*
dtype0*
seed2лВж*
_output_shapes

:*
seed▒ х)
z
dense_2/random_uniform/subSubdense_2/random_uniform/maxdense_2/random_uniform/min*
T0*
_output_shapes
: 
М
dense_2/random_uniform/mulMul$dense_2/random_uniform/RandomUniformdense_2/random_uniform/sub*
T0*
_output_shapes

:
~
dense_2/random_uniformAdddense_2/random_uniform/muldense_2/random_uniform/min*
_output_shapes

:*
T0
м
dense_2/kernelVarHandleOp*!
_class
loc:@dense_2/kernel*
	container *
shape
:*
dtype0*
_output_shapes
: *
shared_namedense_2/kernel
m
/dense_2/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_2/kernel*
_output_shapes
: 
Б
dense_2/kernel/AssignAssignVariableOpdense_2/kerneldense_2/random_uniform*!
_class
loc:@dense_2/kernel*
dtype0
Ф
"dense_2/kernel/Read/ReadVariableOpReadVariableOpdense_2/kernel*
dtype0*
_output_shapes

:*!
_class
loc:@dense_2/kernel
Z
dense_2/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_2/biasVarHandleOp*
dtype0*
_output_shapes
: *
shared_namedense_2/bias*
_class
loc:@dense_2/bias*
	container *
shape:
i
-dense_2/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_2/bias*
_output_shapes
: 
r
dense_2/bias/AssignAssignVariableOpdense_2/biasdense_2/Const*
_class
loc:@dense_2/bias*
dtype0
К
 dense_2/bias/Read/ReadVariableOpReadVariableOpdense_2/bias*
dtype0*
_output_shapes
:*
_class
loc:@dense_2/bias
l
dense_2/MatMul/ReadVariableOpReadVariableOpdense_2/kernel*
dtype0*
_output_shapes

:
Э
dense_2/MatMulMatMuldense_1/Reludense_2/MatMul/ReadVariableOp*
transpose_b( *
T0*
transpose_a( *'
_output_shapes
:         
g
dense_2/BiasAdd/ReadVariableOpReadVariableOpdense_2/bias*
dtype0*
_output_shapes
:
У
dense_2/BiasAddBiasAdddense_2/MatMuldense_2/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_2/ReluReludense_2/BiasAdd*
T0*'
_output_shapes
:         
m
dense_3/random_uniform/shapeConst*
valueB"      *
dtype0*
_output_shapes
:
_
dense_3/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_3/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_3/random_uniform/RandomUniformRandomUniformdense_3/random_uniform/shape*
dtype0*
seed2яЩ╧*
_output_shapes

:*
seed▒ х)*
T0
z
dense_3/random_uniform/subSubdense_3/random_uniform/maxdense_3/random_uniform/min*
T0*
_output_shapes
: 
М
dense_3/random_uniform/mulMul$dense_3/random_uniform/RandomUniformdense_3/random_uniform/sub*
_output_shapes

:*
T0
~
dense_3/random_uniformAdddense_3/random_uniform/muldense_3/random_uniform/min*
T0*
_output_shapes

:
м
dense_3/kernelVarHandleOp*!
_class
loc:@dense_3/kernel*
	container *
shape
:*
dtype0*
_output_shapes
: *
shared_namedense_3/kernel
m
/dense_3/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_3/kernel*
_output_shapes
: 
Б
dense_3/kernel/AssignAssignVariableOpdense_3/kerneldense_3/random_uniform*!
_class
loc:@dense_3/kernel*
dtype0
Ф
"dense_3/kernel/Read/ReadVariableOpReadVariableOpdense_3/kernel*!
_class
loc:@dense_3/kernel*
dtype0*
_output_shapes

:
Z
dense_3/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_3/biasVarHandleOp*
shared_namedense_3/bias*
_class
loc:@dense_3/bias*
	container *
shape:*
dtype0*
_output_shapes
: 
i
-dense_3/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_3/bias*
_output_shapes
: 
r
dense_3/bias/AssignAssignVariableOpdense_3/biasdense_3/Const*
_class
loc:@dense_3/bias*
dtype0
К
 dense_3/bias/Read/ReadVariableOpReadVariableOpdense_3/bias*
_class
loc:@dense_3/bias*
dtype0*
_output_shapes
:
l
dense_3/MatMul/ReadVariableOpReadVariableOpdense_3/kernel*
dtype0*
_output_shapes

:
Э
dense_3/MatMulMatMuldense_2/Reludense_3/MatMul/ReadVariableOp*
T0*
transpose_a( *'
_output_shapes
:         *
transpose_b( 
g
dense_3/BiasAdd/ReadVariableOpReadVariableOpdense_3/bias*
dtype0*
_output_shapes
:
У
dense_3/BiasAddBiasAdddense_3/MatMuldense_3/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_3/ReluReludense_3/BiasAdd*
T0*'
_output_shapes
:         
m
dense_4/random_uniform/shapeConst*
valueB"   
   *
dtype0*
_output_shapes
:
_
dense_4/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_4/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_4/random_uniform/RandomUniformRandomUniformdense_4/random_uniform/shape*
seed▒ х)*
T0*
dtype0*
seed2ы▓¤*
_output_shapes

:

z
dense_4/random_uniform/subSubdense_4/random_uniform/maxdense_4/random_uniform/min*
T0*
_output_shapes
: 
М
dense_4/random_uniform/mulMul$dense_4/random_uniform/RandomUniformdense_4/random_uniform/sub*
_output_shapes

:
*
T0
~
dense_4/random_uniformAdddense_4/random_uniform/muldense_4/random_uniform/min*
T0*
_output_shapes

:

м
dense_4/kernelVarHandleOp*!
_class
loc:@dense_4/kernel*
	container *
shape
:
*
dtype0*
_output_shapes
: *
shared_namedense_4/kernel
m
/dense_4/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_4/kernel*
_output_shapes
: 
Б
dense_4/kernel/AssignAssignVariableOpdense_4/kerneldense_4/random_uniform*
dtype0*!
_class
loc:@dense_4/kernel
Ф
"dense_4/kernel/Read/ReadVariableOpReadVariableOpdense_4/kernel*!
_class
loc:@dense_4/kernel*
dtype0*
_output_shapes

:

Z
dense_4/ConstConst*
valueB
*    *
dtype0*
_output_shapes
:

в
dense_4/biasVarHandleOp*
shape:
*
dtype0*
_output_shapes
: *
shared_namedense_4/bias*
_class
loc:@dense_4/bias*
	container 
i
-dense_4/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_4/bias*
_output_shapes
: 
r
dense_4/bias/AssignAssignVariableOpdense_4/biasdense_4/Const*
dtype0*
_class
loc:@dense_4/bias
К
 dense_4/bias/Read/ReadVariableOpReadVariableOpdense_4/bias*
_class
loc:@dense_4/bias*
dtype0*
_output_shapes
:

l
dense_4/MatMul/ReadVariableOpReadVariableOpdense_4/kernel*
dtype0*
_output_shapes

:

Э
dense_4/MatMulMatMuldense_3/Reludense_4/MatMul/ReadVariableOp*
T0*
transpose_a( *'
_output_shapes
:         
*
transpose_b( 
g
dense_4/BiasAdd/ReadVariableOpReadVariableOpdense_4/bias*
dtype0*
_output_shapes
:

У
dense_4/BiasAddBiasAdddense_4/MatMuldense_4/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         

П
)Adam/iterations/Initializer/initial_valueConst*"
_class
loc:@Adam/iterations*
value	B	 R *
dtype0	*
_output_shapes
: 
з
Adam/iterationsVarHandleOp*
dtype0	*
_output_shapes
: * 
shared_nameAdam/iterations*"
_class
loc:@Adam/iterations*
	container *
shape: 
o
0Adam/iterations/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam/iterations*
_output_shapes
: 
Ч
Adam/iterations/AssignAssignVariableOpAdam/iterations)Adam/iterations/Initializer/initial_value*"
_class
loc:@Adam/iterations*
dtype0	
П
#Adam/iterations/Read/ReadVariableOpReadVariableOpAdam/iterations*
dtype0	*
_output_shapes
: *"
_class
loc:@Adam/iterations
Ш
,Adam/learning_rate/Initializer/initial_valueConst*
dtype0*
_output_shapes
: *%
_class
loc:@Adam/learning_rate*
valueB
 *oГ:
░
Adam/learning_rateVarHandleOp*
dtype0*
_output_shapes
: *#
shared_nameAdam/learning_rate*%
_class
loc:@Adam/learning_rate*
	container *
shape: 
u
3Adam/learning_rate/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam/learning_rate*
_output_shapes
: 
г
Adam/learning_rate/AssignAssignVariableOpAdam/learning_rate,Adam/learning_rate/Initializer/initial_value*
dtype0*%
_class
loc:@Adam/learning_rate
Ш
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*%
_class
loc:@Adam/learning_rate*
dtype0*
_output_shapes
: 
К
%Adam/beta_1/Initializer/initial_valueConst*
_class
loc:@Adam/beta_1*
valueB
 *fff?*
dtype0*
_output_shapes
: 
Ы
Adam/beta_1VarHandleOp*
_class
loc:@Adam/beta_1*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_nameAdam/beta_1
g
,Adam/beta_1/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam/beta_1*
_output_shapes
: 
З
Adam/beta_1/AssignAssignVariableOpAdam/beta_1%Adam/beta_1/Initializer/initial_value*
dtype0*
_class
loc:@Adam/beta_1
Г
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_class
loc:@Adam/beta_1*
dtype0*
_output_shapes
: 
К
%Adam/beta_2/Initializer/initial_valueConst*
_class
loc:@Adam/beta_2*
valueB
 *w╛?*
dtype0*
_output_shapes
: 
Ы
Adam/beta_2VarHandleOp*
_class
loc:@Adam/beta_2*
	container *
shape: *
dtype0*
_output_shapes
: *
shared_nameAdam/beta_2
g
,Adam/beta_2/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam/beta_2*
_output_shapes
: 
З
Adam/beta_2/AssignAssignVariableOpAdam/beta_2%Adam/beta_2/Initializer/initial_value*
_class
loc:@Adam/beta_2*
dtype0
Г
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
dtype0*
_output_shapes
: *
_class
loc:@Adam/beta_2
И
$Adam/decay/Initializer/initial_valueConst*
_class
loc:@Adam/decay*
valueB
 *    *
dtype0*
_output_shapes
: 
Ш

Adam/decayVarHandleOp*
dtype0*
_output_shapes
: *
shared_name
Adam/decay*
_class
loc:@Adam/decay*
	container *
shape: 
e
+Adam/decay/IsInitialized/VarIsInitializedOpVarIsInitializedOp
Adam/decay*
_output_shapes
: 
Г
Adam/decay/AssignAssignVariableOp
Adam/decay$Adam/decay/Initializer/initial_value*
_class
loc:@Adam/decay*
dtype0
А
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_class
loc:@Adam/decay*
dtype0*
_output_shapes
: 
Г
dense_4_targetPlaceholder*
dtype0*0
_output_shapes
:                  *%
shape:                  
q
dense_4_sample_weightsPlaceholder*
dtype0*#
_output_shapes
:         *
shape:         
В
(loss/dense_4_loss/mean_squared_error/subSubdense_4/BiasAdddense_4_target*'
_output_shapes
:         
*
T0
С
+loss/dense_4_loss/mean_squared_error/SquareSquare(loss/dense_4_loss/mean_squared_error/sub*
T0*'
_output_shapes
:         

Ж
;loss/dense_4_loss/mean_squared_error/Mean/reduction_indicesConst*
valueB :
         *
dtype0*
_output_shapes
: 
ц
)loss/dense_4_loss/mean_squared_error/MeanMean+loss/dense_4_loss/mean_squared_error/Square;loss/dense_4_loss/mean_squared_error/Mean/reduction_indices*
T0*#
_output_shapes
:         *

Tidx0*
	keep_dims( 
о
6loss/dense_4_loss/mean_squared_error/weighted_loss/mulMuldense_4_sample_weights)loss/dense_4_loss/mean_squared_error/Mean*#
_output_shapes
:         *
T0
В
8loss/dense_4_loss/mean_squared_error/weighted_loss/ConstConst*
valueB: *
dtype0*
_output_shapes
:
э
6loss/dense_4_loss/mean_squared_error/weighted_loss/SumSum6loss/dense_4_loss/mean_squared_error/weighted_loss/mul8loss/dense_4_loss/mean_squared_error/weighted_loss/Const*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
╡
Dloss/dense_4_loss/mean_squared_error/weighted_loss/num_elements/SizeSize6loss/dense_4_loss/mean_squared_error/weighted_loss/mul*
T0*
out_type0*
_output_shapes
: 
╥
Dloss/dense_4_loss/mean_squared_error/weighted_loss/num_elements/CastCastDloss/dense_4_loss/mean_squared_error/weighted_loss/num_elements/Size*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0
ф
:loss/dense_4_loss/mean_squared_error/weighted_loss/truedivRealDiv6loss/dense_4_loss/mean_squared_error/weighted_loss/SumDloss/dense_4_loss/mean_squared_error/weighted_loss/num_elements/Cast*
_output_shapes
: *
T0
O

loss/mul/xConst*
valueB
 *  А?*
dtype0*
_output_shapes
: 
x
loss/mulMul
loss/mul/x:loss/dense_4_loss/mean_squared_error/weighted_loss/truediv*
T0*
_output_shapes
: 
H
ConstConst*
valueB *
dtype0*
_output_shapes
: 
[
MeanMeanloss/mulConst*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
t
dense_5_inputPlaceholder*
dtype0*)
_output_shapes
:         АА	*
shape:         АА	
m
dense_5/random_uniform/shapeConst*
valueB" @    *
dtype0*
_output_shapes
:
_
dense_5/random_uniform/minConst*
dtype0*
_output_shapes
: *
valueB
 *ь╤╗
_
dense_5/random_uniform/maxConst*
valueB
 *ь╤;*
dtype0*
_output_shapes
: 
к
$dense_5/random_uniform/RandomUniformRandomUniformdense_5/random_uniform/shape*
T0*
dtype0*
seed2│ъ▀* 
_output_shapes
:
АА	*
seed▒ х)
z
dense_5/random_uniform/subSubdense_5/random_uniform/maxdense_5/random_uniform/min*
T0*
_output_shapes
: 
О
dense_5/random_uniform/mulMul$dense_5/random_uniform/RandomUniformdense_5/random_uniform/sub*
T0* 
_output_shapes
:
АА	
А
dense_5/random_uniformAdddense_5/random_uniform/muldense_5/random_uniform/min*
T0* 
_output_shapes
:
АА	
о
dense_5/kernelVarHandleOp*
dtype0*
_output_shapes
: *
shared_namedense_5/kernel*!
_class
loc:@dense_5/kernel*
	container *
shape:
АА	
m
/dense_5/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_5/kernel*
_output_shapes
: 
Б
dense_5/kernel/AssignAssignVariableOpdense_5/kerneldense_5/random_uniform*!
_class
loc:@dense_5/kernel*
dtype0
Ц
"dense_5/kernel/Read/ReadVariableOpReadVariableOpdense_5/kernel*!
_class
loc:@dense_5/kernel*
dtype0* 
_output_shapes
:
АА	
Z
dense_5/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_5/biasVarHandleOp*
_class
loc:@dense_5/bias*
	container *
shape:*
dtype0*
_output_shapes
: *
shared_namedense_5/bias
i
-dense_5/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_5/bias*
_output_shapes
: 
r
dense_5/bias/AssignAssignVariableOpdense_5/biasdense_5/Const*
_class
loc:@dense_5/bias*
dtype0
К
 dense_5/bias/Read/ReadVariableOpReadVariableOpdense_5/bias*
dtype0*
_output_shapes
:*
_class
loc:@dense_5/bias
n
dense_5/MatMul/ReadVariableOpReadVariableOpdense_5/kernel*
dtype0* 
_output_shapes
:
АА	
Ю
dense_5/MatMulMatMuldense_5_inputdense_5/MatMul/ReadVariableOp*
transpose_a( *'
_output_shapes
:         *
transpose_b( *
T0
g
dense_5/BiasAdd/ReadVariableOpReadVariableOpdense_5/bias*
dtype0*
_output_shapes
:
У
dense_5/BiasAddBiasAdddense_5/MatMuldense_5/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_5/ReluReludense_5/BiasAdd*'
_output_shapes
:         *
T0
m
dense_6/random_uniform/shapeConst*
valueB"      *
dtype0*
_output_shapes
:
_
dense_6/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_6/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_6/random_uniform/RandomUniformRandomUniformdense_6/random_uniform/shape*
seed▒ х)*
T0*
dtype0*
seed2┌╓ы*
_output_shapes

:
z
dense_6/random_uniform/subSubdense_6/random_uniform/maxdense_6/random_uniform/min*
_output_shapes
: *
T0
М
dense_6/random_uniform/mulMul$dense_6/random_uniform/RandomUniformdense_6/random_uniform/sub*
T0*
_output_shapes

:
~
dense_6/random_uniformAdddense_6/random_uniform/muldense_6/random_uniform/min*
T0*
_output_shapes

:
м
dense_6/kernelVarHandleOp*
shared_namedense_6/kernel*!
_class
loc:@dense_6/kernel*
	container *
shape
:*
dtype0*
_output_shapes
: 
m
/dense_6/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_6/kernel*
_output_shapes
: 
Б
dense_6/kernel/AssignAssignVariableOpdense_6/kerneldense_6/random_uniform*!
_class
loc:@dense_6/kernel*
dtype0
Ф
"dense_6/kernel/Read/ReadVariableOpReadVariableOpdense_6/kernel*!
_class
loc:@dense_6/kernel*
dtype0*
_output_shapes

:
Z
dense_6/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_6/biasVarHandleOp*
shared_namedense_6/bias*
_class
loc:@dense_6/bias*
	container *
shape:*
dtype0*
_output_shapes
: 
i
-dense_6/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_6/bias*
_output_shapes
: 
r
dense_6/bias/AssignAssignVariableOpdense_6/biasdense_6/Const*
_class
loc:@dense_6/bias*
dtype0
К
 dense_6/bias/Read/ReadVariableOpReadVariableOpdense_6/bias*
_class
loc:@dense_6/bias*
dtype0*
_output_shapes
:
l
dense_6/MatMul/ReadVariableOpReadVariableOpdense_6/kernel*
dtype0*
_output_shapes

:
Э
dense_6/MatMulMatMuldense_5/Reludense_6/MatMul/ReadVariableOp*
transpose_b( *
T0*
transpose_a( *'
_output_shapes
:         
g
dense_6/BiasAdd/ReadVariableOpReadVariableOpdense_6/bias*
dtype0*
_output_shapes
:
У
dense_6/BiasAddBiasAdddense_6/MatMuldense_6/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_6/ReluReludense_6/BiasAdd*'
_output_shapes
:         *
T0
m
dense_7/random_uniform/shapeConst*
dtype0*
_output_shapes
:*
valueB"      
_
dense_7/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_7/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_7/random_uniform/RandomUniformRandomUniformdense_7/random_uniform/shape*
T0*
dtype0*
seed2ЯВ■*
_output_shapes

:*
seed▒ х)
z
dense_7/random_uniform/subSubdense_7/random_uniform/maxdense_7/random_uniform/min*
T0*
_output_shapes
: 
М
dense_7/random_uniform/mulMul$dense_7/random_uniform/RandomUniformdense_7/random_uniform/sub*
T0*
_output_shapes

:
~
dense_7/random_uniformAdddense_7/random_uniform/muldense_7/random_uniform/min*
T0*
_output_shapes

:
м
dense_7/kernelVarHandleOp*
shape
:*
dtype0*
_output_shapes
: *
shared_namedense_7/kernel*!
_class
loc:@dense_7/kernel*
	container 
m
/dense_7/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_7/kernel*
_output_shapes
: 
Б
dense_7/kernel/AssignAssignVariableOpdense_7/kerneldense_7/random_uniform*
dtype0*!
_class
loc:@dense_7/kernel
Ф
"dense_7/kernel/Read/ReadVariableOpReadVariableOpdense_7/kernel*!
_class
loc:@dense_7/kernel*
dtype0*
_output_shapes

:
Z
dense_7/ConstConst*
valueB*    *
dtype0*
_output_shapes
:
в
dense_7/biasVarHandleOp*
dtype0*
_output_shapes
: *
shared_namedense_7/bias*
_class
loc:@dense_7/bias*
	container *
shape:
i
-dense_7/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_7/bias*
_output_shapes
: 
r
dense_7/bias/AssignAssignVariableOpdense_7/biasdense_7/Const*
dtype0*
_class
loc:@dense_7/bias
К
 dense_7/bias/Read/ReadVariableOpReadVariableOpdense_7/bias*
_class
loc:@dense_7/bias*
dtype0*
_output_shapes
:
l
dense_7/MatMul/ReadVariableOpReadVariableOpdense_7/kernel*
dtype0*
_output_shapes

:
Э
dense_7/MatMulMatMuldense_6/Reludense_7/MatMul/ReadVariableOp*
T0*
transpose_a( *'
_output_shapes
:         *
transpose_b( 
g
dense_7/BiasAdd/ReadVariableOpReadVariableOpdense_7/bias*
dtype0*
_output_shapes
:
У
dense_7/BiasAddBiasAdddense_7/MatMuldense_7/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         
W
dense_7/ReluReludense_7/BiasAdd*
T0*'
_output_shapes
:         
m
dense_8/random_uniform/shapeConst*
valueB"   
   *
dtype0*
_output_shapes
:
_
dense_8/random_uniform/minConst*
valueB
 *   ┐*
dtype0*
_output_shapes
: 
_
dense_8/random_uniform/maxConst*
valueB
 *   ?*
dtype0*
_output_shapes
: 
и
$dense_8/random_uniform/RandomUniformRandomUniformdense_8/random_uniform/shape*
seed▒ х)*
T0*
dtype0*
seed2▓п╥*
_output_shapes

:

z
dense_8/random_uniform/subSubdense_8/random_uniform/maxdense_8/random_uniform/min*
T0*
_output_shapes
: 
М
dense_8/random_uniform/mulMul$dense_8/random_uniform/RandomUniformdense_8/random_uniform/sub*
_output_shapes

:
*
T0
~
dense_8/random_uniformAdddense_8/random_uniform/muldense_8/random_uniform/min*
_output_shapes

:
*
T0
м
dense_8/kernelVarHandleOp*
shared_namedense_8/kernel*!
_class
loc:@dense_8/kernel*
	container *
shape
:
*
dtype0*
_output_shapes
: 
m
/dense_8/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_8/kernel*
_output_shapes
: 
Б
dense_8/kernel/AssignAssignVariableOpdense_8/kerneldense_8/random_uniform*
dtype0*!
_class
loc:@dense_8/kernel
Ф
"dense_8/kernel/Read/ReadVariableOpReadVariableOpdense_8/kernel*!
_class
loc:@dense_8/kernel*
dtype0*
_output_shapes

:

Z
dense_8/ConstConst*
valueB
*    *
dtype0*
_output_shapes
:

в
dense_8/biasVarHandleOp*
shared_namedense_8/bias*
_class
loc:@dense_8/bias*
	container *
shape:
*
dtype0*
_output_shapes
: 
i
-dense_8/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOpdense_8/bias*
_output_shapes
: 
r
dense_8/bias/AssignAssignVariableOpdense_8/biasdense_8/Const*
_class
loc:@dense_8/bias*
dtype0
К
 dense_8/bias/Read/ReadVariableOpReadVariableOpdense_8/bias*
dtype0*
_output_shapes
:
*
_class
loc:@dense_8/bias
l
dense_8/MatMul/ReadVariableOpReadVariableOpdense_8/kernel*
dtype0*
_output_shapes

:

Э
dense_8/MatMulMatMuldense_7/Reludense_8/MatMul/ReadVariableOp*
transpose_b( *
T0*
transpose_a( *'
_output_shapes
:         

g
dense_8/BiasAdd/ReadVariableOpReadVariableOpdense_8/bias*
dtype0*
_output_shapes
:

У
dense_8/BiasAddBiasAdddense_8/MatMuldense_8/BiasAdd/ReadVariableOp*
T0*
data_formatNHWC*'
_output_shapes
:         

У
+Adam_1/iterations/Initializer/initial_valueConst*
dtype0	*
_output_shapes
: *$
_class
loc:@Adam_1/iterations*
value	B	 R 
н
Adam_1/iterationsVarHandleOp*
dtype0	*
_output_shapes
: *"
shared_nameAdam_1/iterations*$
_class
loc:@Adam_1/iterations*
	container *
shape: 
s
2Adam_1/iterations/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/iterations*
_output_shapes
: 
Я
Adam_1/iterations/AssignAssignVariableOpAdam_1/iterations+Adam_1/iterations/Initializer/initial_value*$
_class
loc:@Adam_1/iterations*
dtype0	
Х
%Adam_1/iterations/Read/ReadVariableOpReadVariableOpAdam_1/iterations*$
_class
loc:@Adam_1/iterations*
dtype0	*
_output_shapes
: 
Ь
.Adam_1/learning_rate/Initializer/initial_valueConst*'
_class
loc:@Adam_1/learning_rate*
valueB
 *oГ:*
dtype0*
_output_shapes
: 
╢
Adam_1/learning_rateVarHandleOp*'
_class
loc:@Adam_1/learning_rate*
	container *
shape: *
dtype0*
_output_shapes
: *%
shared_nameAdam_1/learning_rate
y
5Adam_1/learning_rate/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/learning_rate*
_output_shapes
: 
л
Adam_1/learning_rate/AssignAssignVariableOpAdam_1/learning_rate.Adam_1/learning_rate/Initializer/initial_value*'
_class
loc:@Adam_1/learning_rate*
dtype0
Ю
(Adam_1/learning_rate/Read/ReadVariableOpReadVariableOpAdam_1/learning_rate*'
_class
loc:@Adam_1/learning_rate*
dtype0*
_output_shapes
: 
О
'Adam_1/beta_1/Initializer/initial_valueConst* 
_class
loc:@Adam_1/beta_1*
valueB
 *fff?*
dtype0*
_output_shapes
: 
б
Adam_1/beta_1VarHandleOp*
shared_nameAdam_1/beta_1* 
_class
loc:@Adam_1/beta_1*
	container *
shape: *
dtype0*
_output_shapes
: 
k
.Adam_1/beta_1/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/beta_1*
_output_shapes
: 
П
Adam_1/beta_1/AssignAssignVariableOpAdam_1/beta_1'Adam_1/beta_1/Initializer/initial_value* 
_class
loc:@Adam_1/beta_1*
dtype0
Й
!Adam_1/beta_1/Read/ReadVariableOpReadVariableOpAdam_1/beta_1*
dtype0*
_output_shapes
: * 
_class
loc:@Adam_1/beta_1
О
'Adam_1/beta_2/Initializer/initial_valueConst* 
_class
loc:@Adam_1/beta_2*
valueB
 *w╛?*
dtype0*
_output_shapes
: 
б
Adam_1/beta_2VarHandleOp*
dtype0*
_output_shapes
: *
shared_nameAdam_1/beta_2* 
_class
loc:@Adam_1/beta_2*
	container *
shape: 
k
.Adam_1/beta_2/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/beta_2*
_output_shapes
: 
П
Adam_1/beta_2/AssignAssignVariableOpAdam_1/beta_2'Adam_1/beta_2/Initializer/initial_value* 
_class
loc:@Adam_1/beta_2*
dtype0
Й
!Adam_1/beta_2/Read/ReadVariableOpReadVariableOpAdam_1/beta_2* 
_class
loc:@Adam_1/beta_2*
dtype0*
_output_shapes
: 
М
&Adam_1/decay/Initializer/initial_valueConst*
_class
loc:@Adam_1/decay*
valueB
 *    *
dtype0*
_output_shapes
: 
Ю
Adam_1/decayVarHandleOp*
shared_nameAdam_1/decay*
_class
loc:@Adam_1/decay*
	container *
shape: *
dtype0*
_output_shapes
: 
i
-Adam_1/decay/IsInitialized/VarIsInitializedOpVarIsInitializedOpAdam_1/decay*
_output_shapes
: 
Л
Adam_1/decay/AssignAssignVariableOpAdam_1/decay&Adam_1/decay/Initializer/initial_value*
_class
loc:@Adam_1/decay*
dtype0
Ж
 Adam_1/decay/Read/ReadVariableOpReadVariableOpAdam_1/decay*
_class
loc:@Adam_1/decay*
dtype0*
_output_shapes
: 
Г
dense_8_targetPlaceholder*
dtype0*0
_output_shapes
:                  *%
shape:                  
q
dense_8_sample_weightsPlaceholder*
dtype0*#
_output_shapes
:         *
shape:         
Д
*loss_1/dense_8_loss/mean_squared_error/subSubdense_8/BiasAdddense_8_target*
T0*'
_output_shapes
:         

Х
-loss_1/dense_8_loss/mean_squared_error/SquareSquare*loss_1/dense_8_loss/mean_squared_error/sub*'
_output_shapes
:         
*
T0
И
=loss_1/dense_8_loss/mean_squared_error/Mean/reduction_indicesConst*
valueB :
         *
dtype0*
_output_shapes
: 
ь
+loss_1/dense_8_loss/mean_squared_error/MeanMean-loss_1/dense_8_loss/mean_squared_error/Square=loss_1/dense_8_loss/mean_squared_error/Mean/reduction_indices*
T0*#
_output_shapes
:         *

Tidx0*
	keep_dims( 
▓
8loss_1/dense_8_loss/mean_squared_error/weighted_loss/mulMuldense_8_sample_weights+loss_1/dense_8_loss/mean_squared_error/Mean*
T0*#
_output_shapes
:         
Д
:loss_1/dense_8_loss/mean_squared_error/weighted_loss/ConstConst*
valueB: *
dtype0*
_output_shapes
:
є
8loss_1/dense_8_loss/mean_squared_error/weighted_loss/SumSum8loss_1/dense_8_loss/mean_squared_error/weighted_loss/mul:loss_1/dense_8_loss/mean_squared_error/weighted_loss/Const*

Tidx0*
	keep_dims( *
T0*
_output_shapes
: 
╣
Floss_1/dense_8_loss/mean_squared_error/weighted_loss/num_elements/SizeSize8loss_1/dense_8_loss/mean_squared_error/weighted_loss/mul*
T0*
out_type0*
_output_shapes
: 
╓
Floss_1/dense_8_loss/mean_squared_error/weighted_loss/num_elements/CastCastFloss_1/dense_8_loss/mean_squared_error/weighted_loss/num_elements/Size*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0
ъ
<loss_1/dense_8_loss/mean_squared_error/weighted_loss/truedivRealDiv8loss_1/dense_8_loss/mean_squared_error/weighted_loss/SumFloss_1/dense_8_loss/mean_squared_error/weighted_loss/num_elements/Cast*
T0*
_output_shapes
: 
Q
loss_1/mul/xConst*
valueB
 *  А?*
dtype0*
_output_shapes
: 
~

loss_1/mulMulloss_1/mul/x<loss_1/dense_8_loss/mean_squared_error/weighted_loss/truediv*
T0*
_output_shapes
: 
J
Const_1Const*
valueB *
dtype0*
_output_shapes
: 
a
Mean_1Mean
loss_1/mulConst_1*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 
L
VarIsInitializedOpVarIsInitializedOp
Adam/decay*
_output_shapes
: 
Q
VarIsInitializedOp_1VarIsInitializedOpAdam_1/beta_2*
_output_shapes
: 
P
VarIsInitializedOp_2VarIsInitializedOpdense_5/bias*
_output_shapes
: 
P
VarIsInitializedOp_3VarIsInitializedOpdense_8/bias*
_output_shapes
: 
R
VarIsInitializedOp_4VarIsInitializedOpdense_6/kernel*
_output_shapes
: 
P
VarIsInitializedOp_5VarIsInitializedOpdense_6/bias*
_output_shapes
: 
O
VarIsInitializedOp_6VarIsInitializedOpAdam/beta_2*
_output_shapes
: 
R
VarIsInitializedOp_7VarIsInitializedOpdense_2/kernel*
_output_shapes
: 
P
VarIsInitializedOp_8VarIsInitializedOpdense_3/bias*
_output_shapes
: 
V
VarIsInitializedOp_9VarIsInitializedOpAdam/learning_rate*
_output_shapes
: 
S
VarIsInitializedOp_10VarIsInitializedOpdense_7/kernel*
_output_shapes
: 
S
VarIsInitializedOp_11VarIsInitializedOpdense_4/kernel*
_output_shapes
: 
Q
VarIsInitializedOp_12VarIsInitializedOpdense_1/bias*
_output_shapes
: 
S
VarIsInitializedOp_13VarIsInitializedOpdense_1/kernel*
_output_shapes
: 
Q
VarIsInitializedOp_14VarIsInitializedOpdense_4/bias*
_output_shapes
: 
S
VarIsInitializedOp_15VarIsInitializedOpdense_8/kernel*
_output_shapes
: 
Q
VarIsInitializedOp_16VarIsInitializedOpAdam_1/decay*
_output_shapes
: 
Q
VarIsInitializedOp_17VarIsInitializedOpdense_7/bias*
_output_shapes
: 
V
VarIsInitializedOp_18VarIsInitializedOpAdam_1/iterations*
_output_shapes
: 
Y
VarIsInitializedOp_19VarIsInitializedOpAdam_1/learning_rate*
_output_shapes
: 
Q
VarIsInitializedOp_20VarIsInitializedOpdense_2/bias*
_output_shapes
: 
P
VarIsInitializedOp_21VarIsInitializedOpAdam/beta_1*
_output_shapes
: 
S
VarIsInitializedOp_22VarIsInitializedOpdense_3/kernel*
_output_shapes
: 
T
VarIsInitializedOp_23VarIsInitializedOpAdam/iterations*
_output_shapes
: 
S
VarIsInitializedOp_24VarIsInitializedOpdense_5/kernel*
_output_shapes
: 
R
VarIsInitializedOp_25VarIsInitializedOpAdam_1/beta_1*
_output_shapes
: 
ь
initNoOp^Adam/beta_1/Assign^Adam/beta_2/Assign^Adam/decay/Assign^Adam/iterations/Assign^Adam/learning_rate/Assign^Adam_1/beta_1/Assign^Adam_1/beta_2/Assign^Adam_1/decay/Assign^Adam_1/iterations/Assign^Adam_1/learning_rate/Assign^dense_1/bias/Assign^dense_1/kernel/Assign^dense_2/bias/Assign^dense_2/kernel/Assign^dense_3/bias/Assign^dense_3/kernel/Assign^dense_4/bias/Assign^dense_4/kernel/Assign^dense_5/bias/Assign^dense_5/kernel/Assign^dense_6/bias/Assign^dense_6/kernel/Assign^dense_7/bias/Assign^dense_7/kernel/Assign^dense_8/bias/Assign^dense_8/kernel/Assign
`
PlaceholderPlaceholder*
shape:
АА	*
dtype0* 
_output_shapes
:
АА	
N
AssignVariableOpAssignVariableOpdense_5/kernelPlaceholder*
dtype0
r
ReadVariableOpReadVariableOpdense_5/kernel^AssignVariableOp*
dtype0* 
_output_shapes
:
АА	
V
Placeholder_1Placeholder*
dtype0*
_output_shapes
:*
shape:
P
AssignVariableOp_1AssignVariableOpdense_5/biasPlaceholder_1*
dtype0
n
ReadVariableOp_1ReadVariableOpdense_5/bias^AssignVariableOp_1*
dtype0*
_output_shapes
:
^
Placeholder_2Placeholder*
dtype0*
_output_shapes

:*
shape
:
R
AssignVariableOp_2AssignVariableOpdense_6/kernelPlaceholder_2*
dtype0
t
ReadVariableOp_2ReadVariableOpdense_6/kernel^AssignVariableOp_2*
dtype0*
_output_shapes

:
V
Placeholder_3Placeholder*
shape:*
dtype0*
_output_shapes
:
P
AssignVariableOp_3AssignVariableOpdense_6/biasPlaceholder_3*
dtype0
n
ReadVariableOp_3ReadVariableOpdense_6/bias^AssignVariableOp_3*
dtype0*
_output_shapes
:
^
Placeholder_4Placeholder*
dtype0*
_output_shapes

:*
shape
:
R
AssignVariableOp_4AssignVariableOpdense_7/kernelPlaceholder_4*
dtype0
t
ReadVariableOp_4ReadVariableOpdense_7/kernel^AssignVariableOp_4*
dtype0*
_output_shapes

:
V
Placeholder_5Placeholder*
dtype0*
_output_shapes
:*
shape:
P
AssignVariableOp_5AssignVariableOpdense_7/biasPlaceholder_5*
dtype0
n
ReadVariableOp_5ReadVariableOpdense_7/bias^AssignVariableOp_5*
dtype0*
_output_shapes
:
^
Placeholder_6Placeholder*
dtype0*
_output_shapes

:
*
shape
:

R
AssignVariableOp_6AssignVariableOpdense_8/kernelPlaceholder_6*
dtype0
t
ReadVariableOp_6ReadVariableOpdense_8/kernel^AssignVariableOp_6*
dtype0*
_output_shapes

:

V
Placeholder_7Placeholder*
shape:
*
dtype0*
_output_shapes
:

P
AssignVariableOp_7AssignVariableOpdense_8/biasPlaceholder_7*
dtype0
n
ReadVariableOp_7ReadVariableOpdense_8/bias^AssignVariableOp_7*
dtype0*
_output_shapes
:

[
Variable/initial_valueConst*
valueB
 *    *
dtype0*
_output_shapes
: 
l
Variable
VariableV2*
shape: *
shared_name *
dtype0*
	container *
_output_shapes
: 
в
Variable/AssignAssignVariableVariable/initial_value*
T0*
_class
loc:@Variable*
validate_shape(*
_output_shapes
: *
use_locking(
a
Variable/readIdentityVariable*
T0*
_class
loc:@Variable*
_output_shapes
: 
]
Variable_1/initial_valueConst*
dtype0*
_output_shapes
: *
valueB
 *    
n

Variable_1
VariableV2*
dtype0*
	container *
_output_shapes
: *
shape: *
shared_name 
к
Variable_1/AssignAssign
Variable_1Variable_1/initial_value*
use_locking(*
T0*
_class
loc:@Variable_1*
validate_shape(*
_output_shapes
: 
g
Variable_1/readIdentity
Variable_1*
T0*
_class
loc:@Variable_1*
_output_shapes
: 
]
Variable_2/initial_valueConst*
valueB
 *    *
dtype0*
_output_shapes
: 
n

Variable_2
VariableV2*
dtype0*
	container *
_output_shapes
: *
shape: *
shared_name 
к
Variable_2/AssignAssign
Variable_2Variable_2/initial_value*
use_locking(*
T0*
_class
loc:@Variable_2*
validate_shape(*
_output_shapes
: 
g
Variable_2/readIdentity
Variable_2*
T0*
_class
loc:@Variable_2*
_output_shapes
: 
n
Total_Reward/Episode/tagsConst*%
valueB BTotal_Reward/Episode*
dtype0*
_output_shapes
: 
p
Total_Reward/EpisodeScalarSummaryTotal_Reward/Episode/tagsVariable/read*
T0*
_output_shapes
: 
`
Max_Q/Episode/tagsConst*
valueB BMax_Q/Episode*
dtype0*
_output_shapes
: 
d
Max_Q/EpisodeScalarSummaryMax_Q/Episode/tagsVariable_1/read*
T0*
_output_shapes
: 
^
step/Episode/tagsConst*
valueB Bstep/Episode*
dtype0*
_output_shapes
: 
b
step/EpisodeScalarSummarystep/Episode/tagsVariable_2/read*
T0*
_output_shapes
: 
R
Placeholder_8Placeholder*
dtype0*
_output_shapes
:*
shape:
R
Placeholder_9Placeholder*
dtype0*
_output_shapes
:*
shape:
S
Placeholder_10Placeholder*
dtype0*
_output_shapes
:*
shape:
Р
AssignAssignVariablePlaceholder_8*
validate_shape(*
_output_shapes
: *
use_locking( *
T0*
_class
loc:@Variable
Ц
Assign_1Assign
Variable_1Placeholder_9*
T0*
_class
loc:@Variable_1*
validate_shape(*
_output_shapes
: *
use_locking( 
Ч
Assign_2Assign
Variable_2Placeholder_10*
use_locking( *
T0*
_class
loc:@Variable_2*
validate_shape(*
_output_shapes
: 
v
Merge/MergeSummaryMergeSummaryTotal_Reward/EpisodeMax_Q/Episodestep/Episode*
N*
_output_shapes
: "&"H
	summaries;
9
Total_Reward/Episode:0
Max_Q/Episode:0
step/Episode:0"Х
trainable_variables¤·
m
dense_1/kernel:0dense_1/kernel/Assign$dense_1/kernel/Read/ReadVariableOp:0(2dense_1/random_uniform:08
^
dense_1/bias:0dense_1/bias/Assign"dense_1/bias/Read/ReadVariableOp:0(2dense_1/Const:08
m
dense_2/kernel:0dense_2/kernel/Assign$dense_2/kernel/Read/ReadVariableOp:0(2dense_2/random_uniform:08
^
dense_2/bias:0dense_2/bias/Assign"dense_2/bias/Read/ReadVariableOp:0(2dense_2/Const:08
m
dense_3/kernel:0dense_3/kernel/Assign$dense_3/kernel/Read/ReadVariableOp:0(2dense_3/random_uniform:08
^
dense_3/bias:0dense_3/bias/Assign"dense_3/bias/Read/ReadVariableOp:0(2dense_3/Const:08
m
dense_4/kernel:0dense_4/kernel/Assign$dense_4/kernel/Read/ReadVariableOp:0(2dense_4/random_uniform:08
^
dense_4/bias:0dense_4/bias/Assign"dense_4/bias/Read/ReadVariableOp:0(2dense_4/Const:08
Г
Adam/iterations:0Adam/iterations/Assign%Adam/iterations/Read/ReadVariableOp:0(2+Adam/iterations/Initializer/initial_value:08
П
Adam/learning_rate:0Adam/learning_rate/Assign(Adam/learning_rate/Read/ReadVariableOp:0(2.Adam/learning_rate/Initializer/initial_value:08
s
Adam/beta_1:0Adam/beta_1/Assign!Adam/beta_1/Read/ReadVariableOp:0(2'Adam/beta_1/Initializer/initial_value:08
s
Adam/beta_2:0Adam/beta_2/Assign!Adam/beta_2/Read/ReadVariableOp:0(2'Adam/beta_2/Initializer/initial_value:08
o
Adam/decay:0Adam/decay/Assign Adam/decay/Read/ReadVariableOp:0(2&Adam/decay/Initializer/initial_value:08
m
dense_5/kernel:0dense_5/kernel/Assign$dense_5/kernel/Read/ReadVariableOp:0(2dense_5/random_uniform:08
^
dense_5/bias:0dense_5/bias/Assign"dense_5/bias/Read/ReadVariableOp:0(2dense_5/Const:08
m
dense_6/kernel:0dense_6/kernel/Assign$dense_6/kernel/Read/ReadVariableOp:0(2dense_6/random_uniform:08
^
dense_6/bias:0dense_6/bias/Assign"dense_6/bias/Read/ReadVariableOp:0(2dense_6/Const:08
m
dense_7/kernel:0dense_7/kernel/Assign$dense_7/kernel/Read/ReadVariableOp:0(2dense_7/random_uniform:08
^
dense_7/bias:0dense_7/bias/Assign"dense_7/bias/Read/ReadVariableOp:0(2dense_7/Const:08
m
dense_8/kernel:0dense_8/kernel/Assign$dense_8/kernel/Read/ReadVariableOp:0(2dense_8/random_uniform:08
^
dense_8/bias:0dense_8/bias/Assign"dense_8/bias/Read/ReadVariableOp:0(2dense_8/Const:08
Л
Adam_1/iterations:0Adam_1/iterations/Assign'Adam_1/iterations/Read/ReadVariableOp:0(2-Adam_1/iterations/Initializer/initial_value:08
Ч
Adam_1/learning_rate:0Adam_1/learning_rate/Assign*Adam_1/learning_rate/Read/ReadVariableOp:0(20Adam_1/learning_rate/Initializer/initial_value:08
{
Adam_1/beta_1:0Adam_1/beta_1/Assign#Adam_1/beta_1/Read/ReadVariableOp:0(2)Adam_1/beta_1/Initializer/initial_value:08
{
Adam_1/beta_2:0Adam_1/beta_2/Assign#Adam_1/beta_2/Read/ReadVariableOp:0(2)Adam_1/beta_2/Initializer/initial_value:08
w
Adam_1/decay:0Adam_1/decay/Assign"Adam_1/decay/Read/ReadVariableOp:0(2(Adam_1/decay/Initializer/initial_value:08
J

Variable:0Variable/AssignVariable/read:02Variable/initial_value:08
R
Variable_1:0Variable_1/AssignVariable_1/read:02Variable_1/initial_value:08
R
Variable_2:0Variable_2/AssignVariable_2/read:02Variable_2/initial_value:08"Л
	variables¤·
m
dense_1/kernel:0dense_1/kernel/Assign$dense_1/kernel/Read/ReadVariableOp:0(2dense_1/random_uniform:08
^
dense_1/bias:0dense_1/bias/Assign"dense_1/bias/Read/ReadVariableOp:0(2dense_1/Const:08
m
dense_2/kernel:0dense_2/kernel/Assign$dense_2/kernel/Read/ReadVariableOp:0(2dense_2/random_uniform:08
^
dense_2/bias:0dense_2/bias/Assign"dense_2/bias/Read/ReadVariableOp:0(2dense_2/Const:08
m
dense_3/kernel:0dense_3/kernel/Assign$dense_3/kernel/Read/ReadVariableOp:0(2dense_3/random_uniform:08
^
dense_3/bias:0dense_3/bias/Assign"dense_3/bias/Read/ReadVariableOp:0(2dense_3/Const:08
m
dense_4/kernel:0dense_4/kernel/Assign$dense_4/kernel/Read/ReadVariableOp:0(2dense_4/random_uniform:08
^
dense_4/bias:0dense_4/bias/Assign"dense_4/bias/Read/ReadVariableOp:0(2dense_4/Const:08
Г
Adam/iterations:0Adam/iterations/Assign%Adam/iterations/Read/ReadVariableOp:0(2+Adam/iterations/Initializer/initial_value:08
П
Adam/learning_rate:0Adam/learning_rate/Assign(Adam/learning_rate/Read/ReadVariableOp:0(2.Adam/learning_rate/Initializer/initial_value:08
s
Adam/beta_1:0Adam/beta_1/Assign!Adam/beta_1/Read/ReadVariableOp:0(2'Adam/beta_1/Initializer/initial_value:08
s
Adam/beta_2:0Adam/beta_2/Assign!Adam/beta_2/Read/ReadVariableOp:0(2'Adam/beta_2/Initializer/initial_value:08
o
Adam/decay:0Adam/decay/Assign Adam/decay/Read/ReadVariableOp:0(2&Adam/decay/Initializer/initial_value:08
m
dense_5/kernel:0dense_5/kernel/Assign$dense_5/kernel/Read/ReadVariableOp:0(2dense_5/random_uniform:08
^
dense_5/bias:0dense_5/bias/Assign"dense_5/bias/Read/ReadVariableOp:0(2dense_5/Const:08
m
dense_6/kernel:0dense_6/kernel/Assign$dense_6/kernel/Read/ReadVariableOp:0(2dense_6/random_uniform:08
^
dense_6/bias:0dense_6/bias/Assign"dense_6/bias/Read/ReadVariableOp:0(2dense_6/Const:08
m
dense_7/kernel:0dense_7/kernel/Assign$dense_7/kernel/Read/ReadVariableOp:0(2dense_7/random_uniform:08
^
dense_7/bias:0dense_7/bias/Assign"dense_7/bias/Read/ReadVariableOp:0(2dense_7/Const:08
m
dense_8/kernel:0dense_8/kernel/Assign$dense_8/kernel/Read/ReadVariableOp:0(2dense_8/random_uniform:08
^
dense_8/bias:0dense_8/bias/Assign"dense_8/bias/Read/ReadVariableOp:0(2dense_8/Const:08
Л
Adam_1/iterations:0Adam_1/iterations/Assign'Adam_1/iterations/Read/ReadVariableOp:0(2-Adam_1/iterations/Initializer/initial_value:08
Ч
Adam_1/learning_rate:0Adam_1/learning_rate/Assign*Adam_1/learning_rate/Read/ReadVariableOp:0(20Adam_1/learning_rate/Initializer/initial_value:08
{
Adam_1/beta_1:0Adam_1/beta_1/Assign#Adam_1/beta_1/Read/ReadVariableOp:0(2)Adam_1/beta_1/Initializer/initial_value:08
{
Adam_1/beta_2:0Adam_1/beta_2/Assign#Adam_1/beta_2/Read/ReadVariableOp:0(2)Adam_1/beta_2/Initializer/initial_value:08
w
Adam_1/decay:0Adam_1/decay/Assign"Adam_1/decay/Read/ReadVariableOp:0(2(Adam_1/decay/Initializer/initial_value:08
J

Variable:0Variable/AssignVariable/read:02Variable/initial_value:08
R
Variable_1:0Variable_1/AssignVariable_1/read:02Variable_1/initial_value:08
R
Variable_2:0Variable_2/AssignVariable_2/read:02Variable_2/initial_value:08nu